import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

/**
 * 2. Create a class stopwatch, a thread responsible for counting up and another for splits/lap times
 * 3 Create a GUI to visualise this
 */
public class StopWatch extends JFrame
{
    // class variables
    private int seconds = 0;
    private int lap = 1;
    private boolean running = false;
    private SecondsCounter counterThread = new SecondsCounter();
    private LapCounter countLaps = new LapCounter();
    private String lapText = "";
    // -----panels/buttons---------------
    private JPanel pane = new JPanel();
    private JLabel secsText = new JLabel();
    private JLabel lapsText = new JLabel();

    private JButton onOffButt = new JButton("Start/Stop");
    private JButton newLap = new JButton("New lap");

    // no argument constructor
    public StopWatch()
    {
        // starts thread to update seconds counter
        counterThread.start();
        // action listeners
        onOffButt.addActionListener(counterThread);
        newLap.addActionListener(countLaps);
        // GUI stuff
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(new Dimension(500, 900));
        this.add(pane);
        // panel stuff
        pane.setLayout(new GridLayout(2, 2));
        pane.add(secsText);
        pane.add(lapsText);
        pane.add(onOffButt);
        pane.add(newLap);
        secsText.setText(String.valueOf(seconds));
        this.setVisible(true);
    }

    /**
     * counts seconds
     */
    class SecondsCounter extends Thread implements ActionListener
    {
        public void run()
        {
            while (true)
            {
                while (running)
                {
                    seconds++;
                    secsText.setText(String.valueOf(seconds));
                    wait(1);
                }
                wait(1);
            }
        }

        /**
         * waits for an input amount of seconds
         */
        public static void wait(int s)
        {
            try
            {
                TimeUnit.SECONDS.sleep(s);
            } catch (InterruptedException ex)
            {
                Thread.currentThread().interrupt();
                ex.printStackTrace();
            }
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            running = !running;
        }
    }

    /**
     * updates lap counter/timer
     */
    class LapCounter extends Thread implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (seconds != 0)
            {
                lapText += "Lap " + lap + " time was " + seconds + " seconds.<br/>";
                lapsText.setText("<html>" + lapText + "</html>");
                System.out.println();
                lap++;
                seconds = -1;
            }
        }
    }

    public static void main(String[] args)
    {
        new StopWatch();
    }
}
