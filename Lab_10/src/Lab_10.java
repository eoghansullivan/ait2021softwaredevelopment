import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * 1. Create a single thread that counts down in SECONDS  from a user defined integer input
 */
public class Lab_10 extends Thread
{
    public String name;

    public Lab_10(String name)
    {
        this.name = name;
    }

    /**
     * queries user for input of seconds, sounds down from that amount of seconds to 0
     */
    public void run()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number to count down from");
        int countFrom = sc.nextInt();
        for (int i = countFrom; i >= 0; i--)
        {
            System.out.println(i);
            wait(1);
        }
    }

    /**
     * waits for an input amount of seconds
     */
    public void wait(int s)
    {
        try
        {
            TimeUnit.SECONDS.sleep(s);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        Lab_10 lab10 = new Lab_10("Part One");
        lab10.start();
    }
}
