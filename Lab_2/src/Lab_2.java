import java.util.Arrays;

public class Lab_2
{

    public static void main(String[] args)
    {
        /*
         * 1. Create a method which reads in an array of chars and finds the index of a
         * certain letter
         */

        char[] arr = "Words".toCharArray();
        char letterIndex = 'o';
        System.out.println("Array of chars: " + String.valueOf(arr));
        System.out.println(getIndex(arr, letterIndex));
        System.out.println();
        /*
         * 2. Create a method similar to that in part 1 which finds the index in a 2D
         * array
         */
        // finds all indices
        System.out.println("2d array index finder");
        System.out.println("2d array:");

        char[] arr2 = "Words".toCharArray();
        char[] arr3 = "Letters".toCharArray();
        char[][] multiArr = {arr2, arr3};
        System.out.println(Arrays.deepToString(multiArr));
        System.out.println();
        char letterIndex2d = 't';
        indexFinder(multiArr, letterIndex2d);
        System.out.println();

        /*
         * 3 Using the methods above, create an application which takes in two words and
         * joins them on a matching char
         */
        System.out.println("Crossword:");
        char[] one = "hello".toCharArray();
        char[] two = "world".toCharArray();
        doCross(one, two, matchChars(one, two));

    }

    /**
     * @param takes a 2d char array and a char to find from it, loops through each
     *              array and prints the indices
     */
    public static void indexFinder(char[][] arr, char find)
    {
        Boolean found = false;
        for (int i = 0; i < arr.length; i++)
        {
            for (int j = 0; j < arr[i].length; j++)
            {
                char isIt = arr[i][j];
                if (isIt == find)
                {
                    System.out.print(arr[i][j] + " is at location " + i + "" + j);
                    System.out.println();
                    found = true;
                }
            }

        }
        if (!found)
        {
            System.out.println("Character not found");
        }
    }

    /**
     * @param takes in two char arrays and returns the index on which they match a
     *              char, or returns a '!' if not
     */
    public static char matchChars(char[] one, char[] two)
    {
        for (int i = 0; i < one.length - 1; i++)
        {
            for (int j = 0; j < two.length - 1; j++)
            {
                if (one[i] == two[j])
                    return one[i];
            }
        }
        return '!';
    }

    /**
     * @param takes a char array and a char to find the index of the char from the
     *              char array
     */
    public static int getIndex(char[] arr, char ind)
    {
        for (int i = 0; i < arr.length - 1; i++)
        {
            if (arr[i] == ind)
            {
                return i;
            }
        }
        return -1;
    }

    /**
     * @param takes a 2d char array, 2 chars and two index offsets, prints the
     *              arrays perpendicular to eachother at the matching offsets
     */
    public static void arraySetup(char[][] arr, char[] one, char[] two, int indX, int indY)
    {
        for (int i = 0; i <= two.length - 1; i++)
        {
            arr[indX][i] = two[i];
        }

        for (int i = 0; i <= one.length - 1; i++)
        {
            arr[i][indY] = one[i];
        }
        System.out.println(Arrays.deepToString(arr).replace("], ", "]\n").replace("[[", "[").replace("]]", "]"));

    }

    /*
     * takes two char arrays, and a char, if the char isn't "!" then calls other
     * methods to result in a 2d array printed at an offset on a matching letter
     */
    public static void doCross(char[] one, char[] two, char get)
    {
        if (get != '!')
        {
            int firstDistance = getIndex(one, get);
            int secondDistance = getIndex(two, get);
            char[][] toPrint = new char[one.length][two.length + secondDistance];
            arraySetup(toPrint, one, two, firstDistance, secondDistance);
        } else
            System.out.println("No matches");
    }

}
