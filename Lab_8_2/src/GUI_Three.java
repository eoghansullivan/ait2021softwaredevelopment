

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class GUI_Three extends JFrame
{
    private DrawingPanel canvas = new DrawingPanel();
    private static Point clickCoord;

    public GUI_Three()
    {
        this.setName("Shapes"); this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.setSize(500, 500); this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init()
    {
        this.add(canvas); this.setBackground(Color.PINK); this.setVisible(true);
    }

    public static void setClickCoord(Point p)
    {
        clickCoord = p;
    }

    public static Point getClickCoord()
    {
        return clickCoord;
    }

    class DrawingPanel extends JPanel
    {

        private static int shapeType = 1; // 1 rectangle, 2 sphere (more to be added)
        private boolean changingBG = false;
        private ArrayList<Shape> shapes = new ArrayList<>();
        private int shapeIndex;
        private MySphere currentSphere;
        private MyRectangle currentRectangle;
        private boolean dragging = false;
        //private ShapeHolder sha;


        public DrawingPanel()
        {
            this.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseDragged(MouseEvent e)
                {
                    System.out.println("Dragging");
                    dragging = true;
                    super.mouseDragged(e);
                    int x = e.getX();
                    int y = e.getY();

                    System.out.println(x+ " " +y);
                    for (Shape sh : shapes)
                    {
                        if(sh.contains(e.getPoint()))
                        {
                            if (sh instanceof MyRectangle)
                            {
                                System.out.println("Changing Background of rectangle");
                                changingBG = true;
                                currentRectangle = (MyRectangle) sh;
                                shapeIndex = shapes.indexOf(sh);
                                repaint();
                            } else if (sh instanceof MySphere)
                            {
                                System.out.println("Changing bg of sphere");
                                currentSphere = (MySphere) sh;
                                changingBG = true;
                                shapeIndex = shapes.indexOf(sh);
                                repaint();
                            }
                        }else
                        {
                            repaint();
                        }

                    }repaint();
                    dragging = false;

                }
            });

            //this.setOpaque(true);
            this.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    super.mouseClicked(e);
                    GUI_Three.setClickCoord(e.getPoint());
                    for (Shape sh : shapes)
                    {
                        if(sh.contains(e.getPoint()))
                        {
                            if (sh instanceof MyRectangle)
                            {
                                System.out.println("Changing Background of rectangle");
                                changingBG = true;
                                currentRectangle = (MyRectangle) sh;
                                shapeIndex = shapes.indexOf(sh);
                                repaint();
                            } else if (sh instanceof MySphere)
                            {
                                System.out.println("Changing bg of sphere");
                                currentSphere = (MySphere) sh;
                                changingBG = true;
                                shapeIndex = shapes.indexOf(sh);
                                repaint();
                            }
                        }else
                        {
                            repaint();
                        }

                    }repaint();
                }
            });
        }

        public static int getShapeType()
        {
            return shapeType;
        }

        public static void setShapeRandomType()
        {
            int pu = 1 + (int) (Math.random() * 2); System.out.println(pu + " is random shape"); shapeType = pu;
        }


        public void paintComponent(Graphics g)
        {
            //super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;

            if (!changingBG)
            {
                //DrawingPanel.setShapeRandomType();

                if (DrawingPanel.getShapeType() == 1)
                {

                    g2d.setColor(Color.BLACK);
                    if (GUI_Three.getClickCoord() != null)
                    {
                        currentRectangle = new MyRectangle();
                        currentRectangle.setRect(GUI_Three.getClickCoord().getX() - 25,
                                GUI_Three.getClickCoord().getY() - 25, 50, 50);
                        g2d.fill(currentRectangle);
                        shapes.add(currentRectangle);

                    }
                } else if (DrawingPanel.getShapeType() == 2)
                {
                    g2d.setColor(Color.BLACK);
                    if (GUI_Three.getClickCoord() != null)
                    {
                        currentSphere = new MySphere();
                        currentSphere.setFrame(GUI_Three.getClickCoord().getX() - 25,
                                GUI_Three.getClickCoord().getY() - 25, 50, 50);
                        g2d.fill(currentSphere);
                        shapes.add(currentSphere);
                    }
                }

            } else
            {
                if (shapes.get(shapeIndex) instanceof MyRectangle)
                {
                    int currentCol = currentRectangle.getBgColour();
                    System.out.println("Current bg colour is " + currentCol);
                    if (currentCol == 0)
                    {
                        g2d.setColor(Color.RED);
                        currentRectangle.setBgColour(1);
                        g2d.fill(currentRectangle);
                    } else if (currentCol == 1)
                    {
                        g2d.setColor(Color.GREEN);
                        currentRectangle.setBgColour(2);
                        g2d.fill(currentRectangle);
                    } else if (currentCol == 2)
                    {
                        g2d.setColor(Color.BLUE);
                        currentRectangle.setBgColour(3);
                        g2d.fill(currentRectangle);
                    } else if (currentCol == 3)
                    {
                        g2d.setColor(Color.BLACK);
                        currentRectangle.setBgColour(0);
                        g2d.fill(currentRectangle);
                    }
                } else if (shapes.get(shapeIndex) instanceof MySphere)
                {
                    int currentCol = currentSphere.getBgColour();
                    System.out.println("Current bg colour is " + currentCol);
                    if (currentCol == 0)
                    {
                        g2d.setColor(Color.RED);
                        currentSphere.setBgColour(1);
                        g2d.fill(currentSphere);
                    } else if (currentCol == 1)
                    {
                        g2d.setColor(Color.GREEN);
                        currentSphere.setBgColour(2);
                        g2d.fill(currentSphere);
                    } else if (currentCol == 2)
                    {
                        g2d.setColor(Color.BLUE);
                        currentSphere.setBgColour(3);
                        g2d.fill(currentSphere);
                    } else if (currentCol == 3)
                    {
                        g2d.setColor(Color.BLACK);
                        currentSphere.setBgColour(0);
                        g2d.fill(currentSphere);
                    }
                }
                changingBG = false;
            }
        }
    }

    class MyRectangle extends Rectangle2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private int bgColour = 0;

        public MyRectangle() {}

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    class MySphere extends Ellipse2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private int bgColour = 0;

        public MySphere() {}

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    class ShapeHolder
    {
        private MyRectangle myTang;
        private MySphere mySph;
        private int type;

        public ShapeHolder(MyRectangle tang)
        {
            myTang = tang;
            this.type = 0;

        }

        public ShapeHolder(MySphere sph)
        {
            mySph = sph;
            this.type = 1;
        }


        public MyRectangle returnRect()
        {
            return myTang;
        }

        public MySphere returnSph()
        {
            return mySph;
        }

        public int getType()
        {
            return type;
        }

    }

    public static void main(String[] args)
    {
        new GUI_Three().init();
    }


}
