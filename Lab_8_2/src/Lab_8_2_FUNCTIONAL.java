import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * 2. Create a GUI with a canvas, click to drop a random shape. If you click the shape it changes colour,
 * if you click the canvas it draws a new random shape and removes the old one.
 */
public class Lab_8_2_FUNCTIONAL extends JFrame
{
    // class variables
    private final DrawingPanel canvas = new DrawingPanel();
    private static Point clickCoord;

    // no argument constructor
    public Lab_8_2_FUNCTIONAL()
    {
        this.setName("Shapes"); this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.setSize(500, 500); this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * initializes GUI
     */
    public void init()
    {
        this.add(canvas); this.setBackground(Color.PINK); this.setVisible(true);
    }

    /**
     * Sets the clicked coordinates
     */
    public static void setClickCoord(Point p)
    {
        clickCoord = p;
    }

    /**
     * gets the clicked coordinates
     */
    public static Point getClickCoord()
    {
        return clickCoord;
    }

    /**
     * canvas to draw shapes on
     */
    static class DrawingPanel extends JPanel
    {

        // class variables
        private static int shapeType = 0; // 1 rectangle, 2 sphere
        private boolean changingBG = false;
        private ShapeHolder sha;
        private final Color[] cols = {Color.BLACK, Color.RED, Color.GREEN, Color.BLUE};


        // no argument constructor
        public DrawingPanel()
        {
            // checks for clicks and check what type of shape, if any is clicked, then calls repaint
            this.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    super.mouseClicked(e);
                    Lab_8_2_FUNCTIONAL.setClickCoord(e.getPoint());


                    if (sha.getType() == 0)
                    {
                        if (sha.returnRect().contains(e.getPoint()))
                        {
                            System.out.println("Changing bg");
                            changingBG = true;
                            repaint();
                        }
                    } else if (sha.getType() == 1)
                    {
                        if (sha.returnSph().contains(e.getPoint()))
                        {
                            System.out.println("Changing bg");
                            changingBG = true;
                            repaint();
                        }
                    }
                    repaint();
                }
            });
        }

        /**
         * returns the type of shape that is clicked
         */
        public static int getShapeType()
        {
            return shapeType;
        }

        /**
         * sets the type of shape to be created
         */
        public static void setShapeRandomType()
        {
            int pu = 1 + (int) (Math.random() * 2); System.out.println(pu + " is random shape"); shapeType = pu;
        }

        /**
         * if the existing shape is clicked, the background colour is changed
         * if the canvas is clicked, a new shape is created
         */
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;

            // if canvas is clicked
            if (!changingBG)
            {
                // set a random type for new shape
                DrawingPanel.setShapeRandomType();

                // if the new shape type is a Rectangle
                if (DrawingPanel.getShapeType() == 1)
                {
                    // create a new shape holder
                    sha = new ShapeHolder(new MyRectangle());
                    g2d.setColor(Color.BLACK);
                    // if the click coordinate exists
                    if (Lab_8_2_FUNCTIONAL.getClickCoord() != null)
                    {
                        // set and fill the location of the new shape
                        sha.returnRect().setRect(Lab_8_2_FUNCTIONAL.getClickCoord().getX() - 25,
                                Lab_8_2_FUNCTIONAL.getClickCoord().getY() - 25, 50, 50);
                        g2d.fill(sha.returnRect());
                    }
                } else if (DrawingPanel.getShapeType() == 2)
                {
                    // if the shape is a Sphere
                    sha = new ShapeHolder(new MySphere());
                    // do same as for rectangle
                    g2d.setColor(Color.BLACK);
                    if (Lab_8_2_FUNCTIONAL.getClickCoord() != null)
                    {
                        sha.returnSph().setFrame(Lab_8_2_FUNCTIONAL.getClickCoord().getX() - 25,
                                Lab_8_2_FUNCTIONAL.getClickCoord().getY() - 25, 50, 50);
                        g2d.fill(sha.returnSph());
                    }
                }

            } else // if clicking an existing shape
            {
                // if the shape is a rectangle
                if (sha.getType() == 0)
                {
                    // get the rectangle colour
                    int currentCol = sha.returnRect().getBgColour();
                    /// loop up to 4 colours
                    if (currentCol > 3)
                    {
                        currentCol = 0;
                    }
                    // paint the rectangle with the new colour
                    g2d.setColor(cols[currentCol]);
                    sha.returnRect().setBgColour(currentCol + 1);
                    g2d.fill(sha.returnRect());
                } else if (sha.getType() == 1) // if the shape is a Sphere
                {
                    // do same as for rectangle
                    int currentCol = sha.returnSph().getBgColour();
                    if (currentCol > 3)
                    {
                        currentCol = 0;
                    }
                    g2d.setColor(cols[currentCol]);
                    sha.returnSph().setBgColour(currentCol + 1);
                    g2d.fill(sha.returnSph());
                }
                changingBG = false;
            }
        }
    }

    /**
     * Extended rectangle to maintain background colour (bad idea)
     */
    static class MyRectangle extends Rectangle2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private int bgColour = 0;

        public MyRectangle() {}

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    /**
     * Extended Sphere to maintain background colour (bad idea)
     */
    static class MySphere extends Ellipse2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private int bgColour = 0;

        public MySphere() {}

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    /**
     * Class for holding shapes (MyRectangle or MySphere)
     * Contains ttype information of the shape as int
     * Has methods to return either shape object
     */
    static class ShapeHolder
    {
        private MyRectangle myTang;
        private MySphere mySph;
        private final int type;

        public ShapeHolder(MyRectangle tang)
        {
            myTang = tang;
            this.type = 0;

        }

        public ShapeHolder(MySphere sph)
        {
            mySph = sph;
            this.type = 1;
        }


        public MyRectangle returnRect()
        {
            return myTang;
        }

        public MySphere returnSph()
        {
            return mySph;
        }

        public int getType()
        {
            return type;
        }

    }

    public static void main(String[] args)
    {
        new Lab_8_2_FUNCTIONAL().init();
    }


}
