public interface Units
{
    public int NUMBER_OF_UNITS_DEFAULT = 3;

    int getUnits();

    void setUnits(int x);
}
