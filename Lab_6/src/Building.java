/*
 * 1. Create a base class Building which implements interfaces for Walls, Roof
 */

public class Building implements Walls, Roof
{
    private int numberOfWalls, numberOfRoofs;

    public Building()
    {
        this.numberOfWalls = NUMBER_OF_WALLS_DEFAULT;
        this.numberOfRoofs = NUMBER_OF_ROOFS_DEFAULT;
    }

    public Building(int wall, int roof)
    {
        this.numberOfWalls = wall;
        this.numberOfRoofs = roof;
    }

    @Override
    public int getWalls()
    {

        return this.numberOfWalls;
    }

    @Override
    public void setWalls(int x)
    {
        this.numberOfWalls = x;
    }

    @Override
    public int getRoofs()
    {

        return this.numberOfRoofs;
    }

    @Override
    public void setRoofs(int x)
    {
        this.numberOfRoofs = x;

    }

    public String toString()
    {
        return "Walls: " + numberOfWalls + "\nRoofs: " + numberOfRoofs + "\n";
    }
}
