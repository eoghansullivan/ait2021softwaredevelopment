public interface Rooms
{
    public int NUMBER_OF_ROOMS_DEFAULT = 7;

    int getRooms();

    void setRooms(int x);
}
