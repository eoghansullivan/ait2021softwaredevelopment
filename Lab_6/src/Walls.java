public interface Walls
{
    public int NUMBER_OF_WALLS_DEFAULT = 4;

    int getWalls();

    void setWalls(int x);
}
