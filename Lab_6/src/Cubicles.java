public interface Cubicles
{
    public int NUMBER_OF_CUBICLES_DEFAULT = 2;

    int getCubicles();

    void setCubicles(int x);
}
