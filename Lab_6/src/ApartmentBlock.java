/*2. Extend Building into House, ApartmentBlock and OfficeBlock,
 * each will have a new interface such as Rooms for House, Units
 * for ApartmentBlock and Cubicles for OfficeBlock*/
public class ApartmentBlock extends Building implements Units
{
    private int numberOfUnits;

    public ApartmentBlock()
    {
        this.numberOfUnits = NUMBER_OF_UNITS_DEFAULT;
    }

    public ApartmentBlock(int wall, int roof, int unit)
    {
        super(wall, roof);
        this.numberOfUnits = unit;
    }

    @Override
    public int getUnits()
    {
        return numberOfUnits;
    }

    @Override
    public void setUnits(int x)
    {
        this.numberOfUnits = x;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Units: " + numberOfUnits;
    }

}
