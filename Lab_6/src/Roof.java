public interface Roof
{
    public int NUMBER_OF_ROOFS_DEFAULT = 1;

    int getRoofs();

    void setRoofs(int x);
}
