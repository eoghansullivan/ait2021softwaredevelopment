/*2. Extend Building into House, ApartmentBlock and OfficeBlock,
 * each will have a new interface such as Rooms for House, Units
 * for ApartmentBlock and Cubicles for OfficeBlock*/
public class House extends Building implements Rooms
{
    private int numberOfRooms;

    public House()
    {
        this.numberOfRooms = NUMBER_OF_ROOMS_DEFAULT;
    }

    public House(int wall, int roof, int room)
    {
        super(wall, roof);
        this.numberOfRooms = room;
    }

    @Override
    public int getRooms()
    {
        return this.numberOfRooms;
    }

    @Override
    public void setRooms(int x)
    {
        this.numberOfRooms = x;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Rooms: " + numberOfRooms;
    }

}
