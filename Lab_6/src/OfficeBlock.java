/*2. Extend Building into House, ApartmentBlock and OfficeBlock,
 * each will have a new interface such as Rooms for House, Units
 * for ApartmentBlock and Cubicles for OfficeBlock*/
public class OfficeBlock extends Building implements Cubicles
{
    private int numberOfCubicles;

    public OfficeBlock()
    {
        this.numberOfCubicles = NUMBER_OF_CUBICLES_DEFAULT;
    }

    public OfficeBlock(int wall, int roof, int cubicles)
    {
        super(wall, roof);
        this.numberOfCubicles = cubicles;
    }

    @Override
    public int getCubicles()
    {
        return this.numberOfCubicles;
    }

    @Override
    public void setCubicles(int x)
    {
        this.numberOfCubicles = x;
    }

    @Override
    public String toString()
    {
        return super.toString() + "Cubicles: " + numberOfCubicles;
    }

}
