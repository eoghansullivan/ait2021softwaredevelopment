import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/*
 * 3. Create a GUI for Creating a new Building that changes based on the type selected.
 */
public class Lab_6_GUI extends JFrame implements ActionListener, WindowListener
{
    private static final long serialVersionUID = 1L;
    // initialize panels
    JPanel buildin = new JPanel();
    JPanel house = new JPanel();
    JPanel apartment = new JPanel();
    JPanel office = new JPanel();
    // variables for output to GUI and for setting building parameters
    private int check = 0;
    private int walls = 0;
    private int roofs = 0;
    private int rooms = 0;
    private int units = 0;
    private int cubicles = 0;
    /*
     * declare & initialize labels for walls, roofs, rooms, units and cubicles to
     * display names and amounts
     */
    private JLabel wallsLabel = new JLabel("Walls: ", SwingConstants.CENTER);
    private JLabel wallAmountLabel = new JLabel(Integer.toString(walls), SwingConstants.CENTER);
    private JLabel roofsLabel = new JLabel("Roofs: ", SwingConstants.CENTER);
    private JLabel roofAmountLabel = new JLabel(Integer.toString(roofs), SwingConstants.CENTER);
    private JLabel roomsLabel = new JLabel("Rooms: ");
    private JLabel roomAmountLabel = new JLabel(Integer.toString(rooms));
    private JLabel unitsLabel = new JLabel("Units: ");
    private JLabel unitAmountLabel = new JLabel(Integer.toString(units));
    private JLabel cubiclesLabel = new JLabel("Cubicles: ");
    private JLabel cubicleAmountLabel = new JLabel(Integer.toString(cubicles));
    // declare and initialize buttons to do stuff
    private JButton houseButton = new JButton("House");
    private JButton apartmentButton = new JButton("Apartment Block");
    private JButton officeButton = new JButton("Office Block");
    private JButton createBuilding = new JButton("Create Building");
    private JButton setWallsBut = new JButton("Set amount of walls");
    private JButton setRoofsBut = new JButton("Set amount of roofs");
    private JButton setRoomsBut = new JButton("Set amount of rooms");
    private JButton setUnitsBut = new JButton("Set amount of units");
    private JButton setCubiclesBut = new JButton("Set amount of cubicles");
    // declare and set size of text fields for inputs
    private JTextField wallsText = new JTextField(20);
    private JTextField roofsText = new JTextField(20);
    private JTextField roomsText = new JTextField(20);
    private JTextField unitsText = new JTextField(20);
    private JTextField cubiclesText = new JTextField(20);
    // default constructor
    public Lab_6_GUI()
    {
        this.setTitle("Buildings");
        this.setSize(600, 700);

    }

    // add listeners for all buttons and call init() method
    public void listeners()
    {
        houseButton.addActionListener(this);
        apartmentButton.addActionListener(this);
        officeButton.addActionListener(this);
        createBuilding.addActionListener(this);

        setWallsBut.addActionListener(this);
        setRoofsBut.addActionListener(this);
        setRoomsBut.addActionListener(this);
        setUnitsBut.addActionListener(this);
        setCubiclesBut.addActionListener(this);

        init();
    }

    // adds the default elements relating to Building and subclasses
    public void init()
    {
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.addWindowListener(this);

        // building related display
        {
            buildin.setLayout(new GridLayout(20, 0));
            this.add(buildin);

            buildin.add(houseButton);
            buildin.add(apartmentButton);
            buildin.add(officeButton);

            buildin.add(wallsLabel);
            buildin.add(wallAmountLabel);

            buildin.add(roofsLabel);
            buildin.add(roofAmountLabel);

            buildin.add(wallsText);
            buildin.add(setWallsBut);

            buildin.add(roofsText);
            buildin.add(setRoofsBut);

            buildin.add(createBuilding);
            this.setVisible(true);
        }

        // house specific display
        {
            buildin.add(house);
            house.add(roomsLabel);
            house.add(roomAmountLabel);
            house.add(roomsText);
            house.add(setRoomsBut);

            house.setVisible(false);
        }

        // apartment specific display
        {
            buildin.add(apartment);
            apartment.add(unitsLabel);
            apartment.add(unitAmountLabel);
            apartment.add(unitsText);
            apartment.add(setUnitsBut);

            apartment.setVisible(false);
        }

        // office specific display
        {
            buildin.add(office);
            office.add(cubiclesLabel);
            office.add(cubicleAmountLabel);
            office.add(cubiclesText);
            office.add(setCubiclesBut);
            office.setVisible(false);
        }

    }

    /**
     * listener to check for buttons pressed
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        // 3 subclass selectors
        if (e.getSource() == houseButton)
        {
            check = 1;
            house.setVisible(true);
            apartment.setVisible(false);
            office.setVisible(false);
        }
        if (e.getSource() == apartmentButton)
        {
            check = 2;
            apartment.setVisible(true);
            house.setVisible(false);
            office.setVisible(false);
        }
        if (e.getSource() == officeButton)
        {
            check = 3;
            office.setVisible(true);
            apartment.setVisible(false);
            house.setVisible(false);
        }

        // button click events to call setters for class variables
        if (e.getSource() == setWallsBut)
        {
            try
            {

                setWalls(validate(Integer.parseInt(wallsText.getText())));

            } catch (IllegalArgumentException e1)
            {
                System.out.println(e1);
            }
        }
        if (e.getSource() == setRoofsBut)
        {

            try
            {

                setRoofs(validate(Integer.parseInt(roofsText.getText())));

            } catch (IllegalArgumentException e1)
            {
                System.out.println(e1);
            }
        }
        if (e.getSource() == setRoomsBut)
        {

            try
            {

                setRooms(validate(Integer.parseInt(roomsText.getText())));

            } catch (IllegalArgumentException e1)
            {
                System.out.println(e1);
            }
        }
        if (e.getSource() == setUnitsBut)
        {

            try
            {

                setUnits(validate(Integer.parseInt(unitsText.getText())));

            } catch (IllegalArgumentException e1)
            {
                System.out.println(e1);
            }
        }
        if (e.getSource() == setCubiclesBut)
        {

            try
            {

                setCubicles(validate(Integer.parseInt(cubiclesText.getText())));

            } catch (IllegalArgumentException e1)
            {
                System.out.println(e1);
            }
        }

        /*
         * calls constructor for specific subclass depending on condition already set up
         * - check int
         */
        if (e.getSource() == createBuilding)
        {
            if (check == 1)
            {
                House h = new House(walls, roofs, rooms);
                System.out.println("House created");
                System.out.println(h.toString());
            } else if (check == 2)
            {
                ApartmentBlock ab = new ApartmentBlock(walls, roofs, units);
                System.out.println("Apartment created");
                System.out.println(ab.toString());
            } else if (check == 3)
            {
                OfficeBlock ob = new OfficeBlock(walls, roofs, cubicles);
                System.out.println("Office created");
                System.out.println(ob.toString());
            } else
            {
                System.out.println("Not created, must pick a sub type");
            }
        }

    }

    // setters for class variables - also set related GUI elements
    public void setWalls(int input)
    {
        this.walls = input;
        wallAmountLabel.setText(Integer.toString(this.walls));
    }

    public void setRoofs(int input)
    {
        this.roofs = input;
        roofAmountLabel.setText(Integer.toString(this.roofs));
    }

    public void setRooms(int input)
    {
        this.rooms = input;
        roomAmountLabel.setText(Integer.toString(this.rooms));
    }

    public void setUnits(int input)
    {
        this.units = input;
        unitAmountLabel.setText(Integer.toString(this.units));
    }

    public void setCubicles(int input)
    {
        this.cubicles = input;
        cubicleAmountLabel.setText(Integer.toString(this.cubicles));
    }

    public int validate(int i)
    {
        Double test = (double) i;
        if (!test.isNaN())
        {
            if (i <= 0)
            {
                throw new IllegalArgumentException("One or more of the arguments is not valid!");
            }
        } else
        {
            throw new IllegalArgumentException("Is not a number");
        }

        return i;
    }

    // methods from window listener
    @Override
    public void windowOpened(WindowEvent e)
    {

    }

    @Override
    public void windowClosing(WindowEvent e)
    {
        System.out.println("Goodbye World");
        System.exit(0);

    }

    @Override
    public void windowClosed(WindowEvent e)
    {

    }

    @Override
    public void windowIconified(WindowEvent e)
    {

    }

    @Override
    public void windowDeiconified(WindowEvent e)
    {

    }

    @Override
    public void windowActivated(WindowEvent e)
    {

    }

    @Override
    public void windowDeactivated(WindowEvent e)
    {

    }

}
