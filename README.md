# README #

### What is this repository for? ###

This repository is for the Software Development module labs

### Labs ###

### [Lab 1](Lab_1/src/Lab_1.java)

1. Create an application which will read a word from the command line, using a method(s) will analyse the string and
   state its length, number of vowels, first letter and last letter.

2. A class [Box](Lab_1/src/Box.java) complete with getter,setter and constructor methods, it should have dimensions,
   name, status(full/empty)

3. A class [LightSwitch](Lab_1/src/LightSwitch.java) which has a status of on/off, it also has a random chance to blow
   while it is being switched on. Use the input of plus to turn on the light and minus to turn off the light. You must
   constantly poll the keyboard for user input. The application ends when the light blows.

### [Lab 2](Lab_2/src/Lab_2.java)

1. Create a method which reads in an array of chars and finds the index of a certain letter.

2. Create a method similar to that in part 1 which finds the index in a 2D array.

3. Using the methods above, create an application which takes in two words and joins them on a matching char.

### [Lab 3](Lab_3/src/Lab_3.java)

In this lab you will download the associated [text file](files/Lab 3.txt) below

1. Count the number of words in the text file (using java)

2. Using previously developed methods count the number of vowels in the file and the average number of vowels per word

3. Print out the words in the file so that words longer than 3 chars will have the first and last letter stay the same
   but the middle letters jumbled

             for example please jumble these letters becomes pealse jmulbe tehse ltetres 

### [Lab 4](Lab_4/src/Lab_4.java)

1. Read in from last week's text file and print out every 5th word.

2. Create an array of 5 files (do not hard code), put one fifth of the words from the text file in each file (You may
   need to reuse the method for string/file details you created last week).

3. Randomly swap the contents of all files,( eg file1 now contains the contents of file3 ....), finally printing the
   statistics for each of the files

### [Lab 5](Lab_5/src/Lab_5.java)

1. Read into an array the values from the lab3.txt file. and print out each value in sorted order. Create a method for
   sorting and reuse the method for swapping
2. Create a class Book which validates the arguments to be non null, book should have a title, publisher, author, price
3. Using the Comparable interface sort an array of Books. Allow change the sort method

### [Lab 6](Lab_6/src/Lab_6.java)

1. Create a base class Building which implements interfaces for Walls, Roof

2. Extend Building into House, ApartmentBlock and OfficeBlock, each will have a new interface such as Rooms for House,
   Units for ApartmentBlock and Cubicles for OfficeBlock

3. Create a GUI for Creating a new Building that changes based on the type selected.

### [Lab 7](Lab_7/src/Lab_7.java)

1. Create a class CurrencyConverter which implements an Interface BaseCurrency(USD), write a test class to convert to
   this base currency from Euro. (You can find current exchange rates on XE.com)


2. Extend the CurrencyConverter class to become MultipleCurrencyConverter modify the interface for the currencies Euro,
   Yuan, and GBP, create a test class for this.


3. Create a GUI which can update a display for all currencies based on the input of an amount in any currency field

### Contribution guidelines ###

Please don't

### Who do I talk to? ###

Eoghan @ a00287845@student.ait.ie