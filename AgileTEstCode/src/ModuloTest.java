public class ModuloTest
{
    public static void main(String[] args)
    {
        boolean isItLeap = false;
        int year = 2000;
        isItLeap = doCheck(1800);
        System.out.println(isItLeap);
    }

    public static boolean doCheck(int year)
    {
        if (year % 4 != 0) {return false;}
        if (year % 400 == 0) {return true;}
        if (year % 100 == 0) {return false;}
        return true;
    }
}
