import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class GUI_Two_Again extends JFrame
{
    private DrawingPanel canvas = new DrawingPanel();
    private static Point clickCoord;

    public GUI_Two_Again()
    {
        this.setName("Shapes"); this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.setSize(500, 500); this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init()
    {
        this.add(canvas); this.setBackground(Color.PINK); this.setVisible(true);
    }

    public static void setClickCoord(Point p)
    {
        clickCoord = p;
    }

    public static Point getClickCoord()
    {
        return clickCoord;
    }

    class DrawingPanel extends JPanel
    {
        private ArrayList<MyRectangle> rectangles = new ArrayList<>();
        private static int shapeType = 0; // 0 rectangle (more to be added)
        private static int clickedLastIndex = -2;
        private static int currentClicked = 0;
        private boolean changingBG = false;
        private  ArrayList<Shape> shapes = new ArrayList<>();


        public DrawingPanel()
        {

            //this.setOpaque(true);
            this.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    super.mouseClicked(e); GUI_Two_Again.setClickCoord(e.getPoint());
                    for (MyRectangle items : rectangles)
                    {
                        if (items.contains(e.getPoint()))
                        {
                            System.out.println("Changing bg"); changingBG = true; currentClicked = items.getId();
                            System.out.println("Just clicked square : " + currentClicked); repaint();
                        }
                    } repaint();
                }
            });
        }

        public static void setShapeRandomType()
        {
            int pu = 1 + (int) (Math.random() * 3); System.out.println(pu + " is random shape"); shapeType = pu;
        }

        public static int getShapeType()
        {
            return shapeType;
        }

        public void paintComponent(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;

            if (!changingBG)
            {
                if (clickedLastIndex >= -1)
                {
                    //DrawingPanel.setShapeRandomType();

                    if (DrawingPanel.getShapeType() == 0)
                    {
                        rectangles.add(new MyRectangle()); if (clickedLastIndex == -1)
                    {
                        clickedLastIndex++;
                    } else
                    {
                        int len = rectangles.size(); clickedLastIndex = rectangles.get(len - 1).rectangleIndexID;
                    } if (GUI_Two_Again.getClickCoord() != null)
                    {
                        rectangles.get(clickedLastIndex).setRect(GUI_Two_Again.getClickCoord().getX() - 25, GUI_Two_Again.getClickCoord().getY() - 25, 50, 50);
                        g2d.fill(rectangles.get(clickedLastIndex));
                    }
                    }
                } else
                {
                    clickedLastIndex = -1;
                }
            } else
            {
                System.out.println("Rectangle " + currentClicked + " was clicked last");
                int currentCol = rectangles.get(currentClicked).getBgColour();
                System.out.println("Current bg colour is " + currentCol); if (currentCol == 0)
            {
                g2d.setColor(Color.RED); rectangles.get(currentClicked).setBgColour(1);
                g2d.fill(rectangles.get(currentClicked));
            } else if (currentCol == 1)
            {
                g2d.setColor(Color.GREEN); rectangles.get(currentClicked).setBgColour(2);
                g2d.fill(rectangles.get(currentClicked));
            } else if (currentCol == 2)
            {
                g2d.setColor(Color.BLUE); rectangles.get(currentClicked).setBgColour(3);
                g2d.fill(rectangles.get(currentClicked));
            } else if (currentCol == 3)
            {
                g2d.setColor(Color.BLACK); rectangles.get(currentClicked).setBgColour(0);
                g2d.fill(rectangles.get(currentClicked));
            } changingBG = false;
            }
        }
    }


    class MyRectangle extends Rectangle2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private static int rectangleCount = 0;
        private int rectangleIndexID;
        private int bgColour = 0;

        public MyRectangle()
        {
            rectangleCount++; this.rectangleIndexID = rectangleCount - 1;
            System.out.println(this.rectangleIndexID + " rectangles");
        }

        public int getId()
        {
            return rectangleIndexID;
        }

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    class MySphere extends Ellipse2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private static int rectangleCount = 0;
        private int sphereIndexID;
        private int bgColour = 0;

        public MySphere()
        {
            rectangleCount++; this.sphereIndexID = rectangleCount - 1;
            System.out.println(this.sphereIndexID + " rectangles");
        }

        public int getId()
        {
            return sphereIndexID;
        }

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    public static void main(String[] args)
    {
        new GUI_Two_Again().init();
    }


}
