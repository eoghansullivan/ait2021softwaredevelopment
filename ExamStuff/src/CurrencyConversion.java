import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

@SuppressWarnings("serial")
public class CurrencyConversion extends JFrame implements ActionListener, WindowListener
{
    private String onClose = "";

    private Container contentPane = this.getContentPane();

    private JLabel euroLabel = new JLabel("Euro"), yuanLabel = new JLabel("Yuan");

    private JTextField euroArea = new JTextField(10), yuanArea = new JTextField(10);

    private double euroValue, yuanValue;

    public CurrencyConversion()
    {
        this.setSize(400, 100);
        this.setResizable(false);

        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        this.addWindowListener(this);

        this.setTitle("Euro - Yuan Converter");
    }

    public static void main(String[] args)
    {
        new CurrencyConversion().init();
    }

    public void init()
    {

        contentPane.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.insets = new Insets(0, 1, 0, 1);

        gbc.gridx = 1;
        gbc.gridy = 1;

        contentPane.add(euroLabel, gbc);

        gbc.gridx = 2;

        contentPane.add(yuanLabel, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.gridx = 1;
        gbc.gridy = 2;

        contentPane.add(euroArea, gbc);

        gbc.gridx = 2;

        contentPane.add(yuanArea, gbc);

        euroArea.addActionListener(this);

        yuanArea.addActionListener(this);

        this.setVisible(true);
    }

    public double convertCash(double d, char c)
    {
        if (c == 'E')
        {
            double re = Math.round((d * 7.2426214) * 100.) / 100.00;
            onClose = (d + " euro is " + re + " yuan");
            return re;

        } else
        {
            double re = Math.round((d * 0.13806589) * 100.) / 100.00;
            onClose = (d + " yuan is " + re + " euro");
            return re;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == euroArea)
        {
            euroValue = Double.parseDouble(euroArea.getText());
            yuanValue = convertCash(euroValue, 'E');
        } else
        {
            yuanValue = Double.parseDouble(yuanArea.getText());
            euroValue = convertCash(yuanValue, 'D');
        }

        euroArea.setText("" + euroValue);
        yuanArea.setText("" + yuanValue);

    }

    @Override
    public void windowOpened(WindowEvent e)
    {
        System.out.println("Window starts");
    }

    @Override
    public void windowClosing(WindowEvent e)
    {
        try
        {
            PrintWriter pw = new PrintWriter(new File("Exit_Converter.txt"));
            pw.println(onClose);
            pw.close();
        } catch (IOException ex)
        {
            ex.printStackTrace();
        }
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e)
    {

    }

    @Override
    public void windowIconified(WindowEvent e)
    {

    }

    @Override
    public void windowDeiconified(WindowEvent e)
    {

    }

    @Override
    public void windowActivated(WindowEvent e)
    {

    }

    @Override
    public void windowDeactivated(WindowEvent e)
    {

    }
}
