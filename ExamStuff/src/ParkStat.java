import java.util.ArrayList;
//﻿OBJECTID,Name,Address,Website,Type,MUGA,Longitude,Latitude

public class ParkStat implements Comparable<ParkStat>
{

    private static int sortingMethod = 0;
    private static int typeWalk = 0, typePark = 0, typeCemetery = 0, typePlayground = 0;
    private int OBJECTID1;
    private String name2, address3, website4, type5, MUGA6;
    private double longitude7, latitude;

    public ParkStat(String s)
    {
        // System.out.println(s);
        Boolean nameQuote;
        if (s.charAt(2) == '\"')
        {
            nameQuote = true;
        } else
        {
            nameQuote = false;
        }
        Boolean quoted = false;
        int commas = 0;
        if (s.contains("\""))
        {
            quoted = true;
        }
        for (int i = 0; i < s.length(); i++)
        {
            if (s.charAt(i) == ',')
            {
                commas++;
            }
        }
        // System.out.println(commas);

        String[] list = s.split(",");
        for (int i = 0; i < list.length; i++)
        {
            list[i] = list[i].replaceAll(",", "");
            list[i] = list[i].replaceAll("\"", "");
        }
        try
        {
            if (nameQuote)
            {
                this.OBJECTID1 = Integer.parseInt(list[0]);
                this.name2 = list[1] + " ," + list[2];
                this.address3 = list[3];
                this.website4 = list[4];
                this.type5 = list[5];
                this.MUGA6 = list[6];
                this.longitude7 = Double.parseDouble(list[7]);
                this.latitude = Double.parseDouble(list[8]);
            } else if (quoted && commas == 8)
            {
                this.OBJECTID1 = Integer.parseInt(list[0]);
                this.name2 = list[1];
                this.address3 = list[2] + " ," + list[3];
                this.website4 = list[4];
                this.type5 = list[5];
                this.MUGA6 = list[6];
                this.longitude7 = Double.parseDouble(list[7]);
                this.latitude = Double.parseDouble(list[8]);
            } else if (quoted && commas == 9)
            {
                this.OBJECTID1 = Integer.parseInt(list[0]);
                this.name2 = list[1];
                this.address3 = list[2] + " ," + list[3] + " ," + list[4];
                this.website4 = list[5];
                this.type5 = list[6];
                this.MUGA6 = list[7];
                this.longitude7 = Double.parseDouble(list[8]);
                this.latitude = Double.parseDouble(list[9]);
            } else
            {
                this.OBJECTID1 = Integer.parseInt(list[0]);
                this.name2 = list[1];
                this.address3 = list[2];
                this.website4 = list[3];
                this.type5 = list[4];
                this.MUGA6 = list[5];
                this.longitude7 = Double.parseDouble(list[6]);
                this.latitude = Double.parseDouble(list[7]);
            }
        } catch (NumberFormatException nfex)
        {
            System.out.println("Fecked");
        }
        // System.out.println(type5);
        if (this.type5.compareTo("Park") == 0)
        {
            typePark++;
        } else if (this.type5.compareTo("Playground") == 0)
        {
            typePlayground++;
        } else if (this.type5.compareTo("Cemetery") == 0)
        {
            typeCemetery++;
        } else if (this.type5.compareTo("Walk") == 0)
        {
            typeWalk++;
        }

    }

    public static String typeOut()
    {
        return "Parks: " + typePark + "\nWalks: " + typeWalk + "\nPlaygrounds: " + typePlayground + "\nCemeteries:"
                + typeCemetery;
    }

    public static void setSortingMethod(int set)
    {
        sortingMethod = set;
    }

    public static void sort(ArrayList<ParkStat> parkArr)
    {
        for (int i = 0; i < parkArr.size() - 1; i++)
        {
            int leastIndex = i;

            for (int j = i + 1; j < parkArr.size(); j++)
            {
                if (parkArr.get(j).compareTo(parkArr.get(leastIndex)) == 1)
                {
                    leastIndex = j;
                }

            }
            if (leastIndex != i)
            {
                swap(parkArr, i, leastIndex);
            }
        }
    }

    public static void swap(ArrayList<ParkStat> a, int i, int j)
    {
        ParkStat temp = a.get(i);
        a.set(i, a.get(j));
        a.set(j, temp);
    }

    public String toString()
    {
        return "OBJECTID: " + this.OBJECTID1 + "\nName: " + this.name2 + "\nAddress: " + this.address3 + "\nWebsite: "
                + this.website4 + "\nType: " + this.type5 + "\nMUGA: " + this.MUGA6 + "\nLongitude: " + this.longitude7
                + "\nLatitude: " + this.latitude + "\n";
    }

    @Override
    public int compareTo(ParkStat o)
    {
        if (sortingMethod == 1)
        {
            if (this.name2.compareTo(o.name2) < 0)
            {
                return 1;
            }
        } else if (sortingMethod == 2)
        {
            if (this.type5.compareTo(o.type5) < 0)
            {
                return 1;
            }
        } else
        {
            if (this.OBJECTID1 < o.OBJECTID1)
            {
                return 1;
            }
        }

        return 0;
    }

}
