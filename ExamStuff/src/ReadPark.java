import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadPark
{

    private static ArrayList<ParkStat> stat = new ArrayList<>();

    public static void readFile(String filename)
    {
        try
        {
            Scanner sc = new Scanner(new File(filename));

            if (sc.hasNextLine())
                sc.nextLine();// skips heading

            while (sc.hasNextLine())
            {

                stat.add(new ParkStat(sc.nextLine()));
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {

        readFile("text.txt");
        ParkStat.setSortingMethod(1);
        ParkStat.sort(stat);
        printList();

        ParkStat.setSortingMethod(2);
        System.out.println(ParkStat.typeOut());

    }

    public static void printList()
    {
        for (ParkStat s : stat)
        {
            System.out.println(s);
        }
    }
}
