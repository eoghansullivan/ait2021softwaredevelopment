import java.util.ArrayList;

public class FireStationStat implements Comparable<FireStationStat>
{

    private static int sortingMethod = 0;

    private int year1, fires4, incidents5, falseAlarms6, total7, ambulanceTurnouts, OBJECTID;
    private String name2, stationOrPost3;

    public FireStationStat(String s)
    {

        String[] list = s.split(",");
        for (int i = 0; i < list.length; i++)
        {
            list[i] = list[i].replaceAll(",", "");
        }

        try
        {
            this.year1 = Integer.parseInt(list[0]);
            this.name2 = list[1];
            this.stationOrPost3 = list[2];
            this.fires4 = Integer.parseInt(list[3]);
            this.incidents5 = Integer.parseInt(list[4]);
            this.falseAlarms6 = Integer.parseInt(list[5]);
            this.total7 = Integer.parseInt(list[6]);
            this.ambulanceTurnouts = Integer.parseInt(list[7]);
            this.OBJECTID = Integer.parseInt(list[8]);
        } catch (NumberFormatException nfex)
        {
            System.out.println("SOmething is fecked");
        }
    }

    public static void setSortingMethod(int set)
    {
        sortingMethod = set;
    }

    public static void sort(ArrayList<FireStationStat> fireArr)
    {
        for (int i = 0; i < fireArr.size() - 1; i++)
        {
            int leastIndex = i;

            for (int j = i + 1; j < fireArr.size(); j++)
            {
                if (fireArr.get(j).compareTo(fireArr.get(leastIndex)) == 1)
                {
                    leastIndex = j;
                }

            }
            if (leastIndex != i)
            {
                swap(fireArr, i, leastIndex);
            }
        }
    }

    public static void swap(ArrayList<FireStationStat> a, int i, int j)
    {
        FireStationStat temp = a.get(i);
        a.set(i, a.get(j));
        a.set(j, temp);
    }

    public String toString()
    {
        return "Year: " + year1 + "\nName of Fire Station or Fire Post: " + name2 + "\nStation or Post: "
                + stationOrPost3 + "\nFires: " + fires4 + "\nSpecial Service Incidents: " + incidents5
                + "\nFalse Alarms: " + falseAlarms6 + "\nTotal: " + total7 + "\nAmbulance Turnouts: "
                + ambulanceTurnouts + "\nOBJECTID: " + OBJECTID + "\n\n";
    }

    public String toFiresString()
    {
        return "Name of Fire Station or Fire Post: " + name2 + "\nAmount of fires: " + fires4 + "\n";
    }

    @Override
    public int compareTo(FireStationStat o)
    {
        if (sortingMethod == 1)
        {
            if (this.name2.compareTo(o.name2) < 0)
            {
                return 1;
            }
        } else if (sortingMethod == 2)
        {
            if (this.fires4 < o.fires4)
            {
                return 1;
            }
        } else
        {
            if (this.year1 < o.year1)
            {
                return 1;
            }
        }

        return 0;
    }

}
