import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class MouseTester extends JFrame {

    private DrawingCanvas canvas = new DrawingCanvas();


    public MouseTester() {
        this.setSize(new Dimension(700, 700));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init() {
        this.add(canvas);
        this.addMouseListener(canvas);
        this.addMouseMotionListener(canvas);
        canvas.setBackground(Color.BLUE);
        this.setVisible(true);
        canvas.sett();
    }

    class DrawingCanvas extends JPanel implements MouseListener, MouseMotionListener {
        private Point sp;
        private Point p;
        private int scale = 1;

        private boolean holding;
        private boolean toPaint;

        public void sett() {
            System.out.println("Got setup");
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.BLACK);
            if (toPaint) {

                g2d.fillRect(sp.x-(25*scale), sp.y-(50*scale), 50 * scale, 50 * scale);
                toPaint = false;
            }


        }

        @Override
        public void mouseClicked(MouseEvent e) {
            System.out.println("CLICK");
            scale = 1;
            toPaint = true;
            repaint();

        }

        @Override
        public void mousePressed(MouseEvent e) {
            holding = true;
            System.out.println("HOLDING");
            sp = e.getPoint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            System.out.println("NOT HOLDING");
            holding = false;
        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (holding) {
                p = e.getPoint();
                scale = (p.x - sp.x) / 30;
                System.out.println(scale);
                toPaint = true;
                repaint();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {

        }
    }

    public static void main(String[] args) {
        new MouseTester().init();
    }


}
