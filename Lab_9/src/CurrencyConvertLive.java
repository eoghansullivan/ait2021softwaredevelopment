
import lab_7.MultipleCurrencyConverter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 1. Create or extend the currency converter GUI to use the Jsoup library to convert between the four currencies
 */
public class CurrencyConvertLive extends MultipleCurrencyConverter
{
    /**
     * converts euro to usd
     */
    @Override
    public void eurToUSD(double eurIn)
    {
        setUsd_value(round(eurIn / getConversionRate("EUR")));
    }

    /**
     * converts usd to euro
     */
    @Override
    public double usdToEur()
    {
        return round(super.getUsd_value() * getConversionRate("EUR"));
    }

    /**
     * converts GBP to USD
     */
    @Override
    public void gbpToUSD(double gbpIn) {setUsd_value(round(gbpIn / getConversionRate("GBP")));}

    /**
     * converts USD to GBP
     */
    @Override
    public double usdToGBP() {return round(super.getUsd_value() * getConversionRate("GBP"));}

    /**
     * converts YUAN to USD
     */
    @Override
    public void yuanToUSD(double yuanIn) {super.setUsd_value(round(yuanIn / getConversionRate("YUAN")));}

    /**
     * converts USD to YUAN
     */
    @Override
    public double usdToYuan() {return round(super.getUsd_value() * getConversionRate("CNY"));}


    /**
     * Takes a currency type String and parses xe.com for the rate using JSoup
     * returns rate
     */
    public double getConversionRate(String currencyType)
    {
        try
        {
            Document doc = Jsoup.connect(String.format("https://www.xe.com/currencyconverter/convert/?Amount=%f&From=%s&To=%s", 1.0, "USD", currencyType)).get();
            Elements elements = doc.select("p");
            for (Element element : elements)
            {
                String classes = element.className();
                if (classes.contains("result__BigRate"))
                {
                    return Double.parseDouble(element.text().replaceAll("[a-zA-Z]", ""));
                }
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return -1;
    }
}
