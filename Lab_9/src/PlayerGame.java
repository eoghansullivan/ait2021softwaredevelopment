import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Random;

/**
 * 3. Extend the functionality of part 2, create an Arraylist of random shapes
 * once the player is above the shape pressing space will eliminate the shape,
 * the application ends when all shapes are consumed
 */
public class PlayerGame extends PlayerMover
{
    // class variables
    private final CanvasExt canvas = new CanvasExt();
    private final ArrayList<Shape> shapes = new ArrayList<>();

    // no argument constructor
    public PlayerGame()
    {
        super();
        this.setLocationRelativeTo(null);
        this.setTitle("Game");
    }

    // initialize GUI
    public void init()
    {
        gbc.ipadx = 1000;
        gbc.ipady = 1000;
        this.add(canvas, gbc);

        this.canvas.setBackground(Color.BLUE);
        this.addKeyListener(canvas);
        canvas.repaint();

        this.pack();
        this.setVisible(true);
        addOvals();
    }

    // adds 10 random shapes to the canvas in random locations
    public void addOvals()
    {
        Random ra = new Random();
        // canvas height / width
        int height = canvas.getHeight();
        int width = canvas.getWidth();

        int x = 0;
        int y = 0;
        // size of shapes
        int[] shaSize = {50, 50};

        // populates ArrayList with random shapes within canvas bounds
        for (int i = 0; i < 10; i++)
        {
            x = getNewPos(ra, width, x);

            y = getNewPos(ra, height, y);

            int shapeType = ra.nextInt(3);
            if (shapeType == 0)
            {
                shapes.add(new Rectangle2D.Double(x, y, shaSize[0], shaSize[1]));
            } else if (shapeType == 1)
            {
                shapes.add(new Ellipse2D.Double(x, y, shaSize[0], shaSize[1]));
            } else
            {
                shapes.add(new RoundRectangle2D.Double(x, y, shaSize[0], shaSize[1], 20, 20));
            }
        }
        checkTouching();

        canvas.repaint();
    }

    /**
     * Cycles through new random coordinates until the coordinate is safely within bounds
     */
    private int getNewPos(Random ra, int pos, int point)
    {
        int boundX = ra.nextInt(pos - 25);
        if (boundX > 0)
        {
            point = boundX;
            if (point < 25)
            {
                point = 25;
            }
            if (point > pos - 100)
            {
                point = pos - 250;
            }
        }
        return point;
    }

    // canvas for player / game to occur on
    class CanvasExt extends PlayerCanvas
    {
        private boolean pressed = false;

        @Override
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.BLACK);
            collider(g2d);
        }

        // repaints the canvas
        @Override
        public void keyReleased(KeyEvent e)
        {
            super.keyReleased(e);
            if (e.getKeyCode() == 32)
            {
                System.out.println("press");
                canvas.repaint();
                pressed = true;
            } else
            {
                pressed = false;
            }
        }

        /**
         * fills shapes in ArrayList, deletes element if it intersects with a shape and space key is pressed
         */
        public void collider(Graphics2D g2d)
        {
            try
            {
                for (Shape s : shapes)
                {
                    g2d.fill(s);
                    if (pressed)
                    {
                        if (s.intersects(player.getBounds2D()))
                        {
                            shapes.remove(s);
                            canvas.repaint();
                            pressed = false;
                            if (shapes.size() == 0)
                            {
                                System.exit(0);
                            }
                        }
                    }
                }
            } catch (ConcurrentModificationException c)
            {
                canvas.repaint();
            }
        }
    }

    /**
     * attempts to verify that each element of the ArrayList of shapes does not touch any other element
     * remakes list if there are touching elements
     */
    public void checkTouching()
    {
        try
        {
            for (Shape s : shapes)
            {
                for (Shape ss : shapes)
                {
                    if (s != ss)
                    {
                        if (s.contains(ss.getBounds2D()))
                        {
                            // removes touching shapes to attempt to mitigate the ConcurrentModificationException
                            shapes.remove(s);
                            shapes.remove(ss);

                            // empties ArrayList, repaints the canvas and then recursively calls the original calling method
                            shapes.clear();
                            canvas.repaint();
                            addOvals();
                        }
                    }
                }
            }
        } catch (ConcurrentModificationException cc)
        {
            System.out.println("F");
        }
    }

    public static void main(String[] args)
    {
        new PlayerGame().init();
    }
}
