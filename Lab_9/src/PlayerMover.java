import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;

/**
 * 2. Create a circle(player) on a canvas, allow the player to move with wasd keys.
 */
public class PlayerMover extends JFrame
{

    // class variables
    protected PlayerCanvas canvas = new PlayerCanvas();
    protected GridBagConstraints gbc = new GridBagConstraints();

    // constructor with no arguments
    public PlayerMover()
    {
        this.setTitle("Player Mover");
        this.setLayout(new GridBagLayout());
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setSize(new Dimension(650, 650));
    }

    // initialize GUI
    public void init()
    {
        gbc.ipadx = 1000;
        gbc.ipady = 1000;
        this.add(canvas, gbc);

        canvas.setBackground(Color.GREEN);
        this.addKeyListener(canvas);
        canvas.repaint();

        this.pack();
        this.setVisible(true);
    }


    /**
     * Canvas for player to move around on
     */
    static class PlayerCanvas extends JPanel implements KeyListener
    {
        protected Ellipse2D.Double player = new Ellipse2D.Double(50, 50, 50, 50);

        @Override
        public void keyTyped(KeyEvent e)
        {
        }

        @Override
        public void keyPressed(KeyEvent e)
        {
        }

        // key listener check action
        @Override
        public void keyReleased(KeyEvent e)
        {
            movePlayer(e);
        }

        // repaints the player
        @Override
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d.setColor(Color.YELLOW);
            g2d.fill(player);
        }

        // changes player coordinates depending on which key is pressed (w,a,s,d)
        public void movePlayer(KeyEvent e)
        {
            // class variables
            int speed = 30;
            if (e.getKeyCode() == 65)// left
            {
                player = new Ellipse2D.Double(player.x - speed, player.y, 50, 50);
            }
            if (e.getKeyCode() == 87)// up
            {
                player = new Ellipse2D.Double(player.x, player.y - speed, 50, 50);
            }
            if (e.getKeyCode() == 68)// right
            {
                player = new Ellipse2D.Double(player.x + speed, player.y, 50, 50);
            }
            if (e.getKeyCode() == 83)// down
            {
                player = new Ellipse2D.Double(player.x, player.y + speed, 50, 50);
            }
            repaint();
        }
    }
}
