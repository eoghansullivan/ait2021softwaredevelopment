import lab_7.GUI_Controller;
import lab_7.GUI_View;

public class Lab_9
{
    public static void main(String[] args)
    {
        //  extended converter with JSoup
        CurrencyConvertLive mcc = new CurrencyConvertLive();
        GUI_View view = new GUI_View();
        new GUI_Controller(view, mcc);

        // player mover
        new PlayerMover().init();

        // player eats shapes
        new PlayerGame().init();

    }
}
