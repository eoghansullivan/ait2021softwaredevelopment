import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Lab_5
{

    public static void main(String[] args)
    {
        /*
         * 1. Read into an array the values from the lab3.txt file. and print out each
         * value in sorted order. Create a method for sorting and reuse the method for
         * swapping
         */
        sort(fileToArr(new File("Lab 3.txt")));

        /*
         * 3. Using the Comparable interface sort an array of Books. Allow change the
         * sort method
         */
        bookTester();

    }

    /**
     * takes a file and returns a string array containing the elements from the file
     *
     * @param file - the file to be read
     * @return - the string array
     */
    public static String[] fileToArr(File file)
    {
        String s = "";
        Scanner sc;
        String[] arr = null;

        try
        {
            sc = new Scanner(file);

            // appends each next element to a String
            while (sc.hasNext())
            {
                s += sc.next() + " ";
            }

            // creates string array to count
            arr = s.trim().split("\\s+");

            sc.close();

            // removes non letters
            for (int i = 0; i < arr.length; i++)
            {
                arr[i] = arr[i].replaceAll("[^a-zA-Z0-9]", "");

            }

            // removes empty element from array
            String[] removedEmpty = Arrays.stream(arr).filter(x -> !x.isEmpty()).toArray(String[]::new);

            return removedEmpty;

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

        return arr;

    }

    /**
     * sorts a string array by descending order and prints the result
     *
     * @param sArr - string array to be sorted
     */
    public static void sort(String[] sArr)
    {
        // outer loop iterates each array element
        for (int i = 0; i < sArr.length - 1; i++)
        {
            int smallest = i;

            // inner loop compares the next element with the previous and swaps if it is
            // smaller
            for (int j = i + 1; j < sArr.length; j++)
            {
                if (sArr[j].charAt(0) < sArr[smallest].charAt(0))
                {
                    smallest = j;
                }
            }
            if (smallest != i)
            {
                swap(sArr, i, smallest);
            }
        }
        System.out.println("//Part 1");
        System.out.println(Arrays.toString(sArr));
    }

    /**
     * sorts an array of books using the compareTo method on the string or int
     * selected by the exterior compareTo method
     *
     * @param bookArr - the array to be sorted
     */
    public static void sort(Book[] bookArr)
    {
        for (int i = 0; i < bookArr.length - 1; i++)
        {
            int leastIndex = i;

            for (int j = i + 1; j < bookArr.length; j++)
            {
                if (bookArr[j].compareTo(bookArr[leastIndex]) == 1)
                {
                    leastIndex = j;
                }

            }
            if (leastIndex != i)
            {
                swap(bookArr, i, leastIndex);
            }
        }
    }

    /**
     * simple swap
     */
    public static void swap(String[] a, int i, int j)
    {
        String temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void swap(Book[] a, int i, int j)
    {
        Book temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void bookTester()
    {
        System.out.println("\n//Part 3");

        // creates books
        Book b1 = new Book("New Tales of Java Pain", "Hands Inc", "Brain Murphy", 20);
        Book b2 = new Book("Old Tales of Java Gain", "Party Corp", "Alison Lipron", 16);
        Book b3 = new Book("Jiffy, Liffy, Giffy", "Old Town Publishing", "Fifel Peacher", 4);

        // creates and populates book array
        Book[] bookArr = new Book[3];
        bookArr[0] = b1;
        bookArr[1] = b2;
        bookArr[2] = b3;

        // sets sort by author name and sorts
        b1.setSortingMethod(3);
        sort(bookArr);

        // prints books toStrings
        System.out.println(bookArr[0].toString());
        System.out.println(bookArr[1].toString());
        System.out.println(bookArr[2].toString());
    }

}
