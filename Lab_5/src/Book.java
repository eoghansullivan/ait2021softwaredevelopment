/*
 * 2.Create a class Book which validates the arguments to be non null
 * book should have a title, publisher, author, price
 */
public class Book implements Comparable<Book>
{
    private static int sortingMethod = 0;
    private String title, publisher, author;
    private double price;

    public Book(String title, String publisher, String author, double price)
    {
        this.title = validate(title);
        this.publisher = validate(publisher);
        this.author = validate(author);
        this.price = validate(price);
    }

    // validates arguments
    public String validate(String s)
    {
        if (s == null)
        {
            throw new IllegalArgumentException("One or more of the arguments is not valid!");
        }
        return s;
    }

    public int validate(int i)
    {
        if (i <= 0)
        {
            throw new IllegalArgumentException("One or more of the arguments is not valid!");
        }
        return i;
    }

    public double validate(double i)
    {
        if (i <= 0)
        {
            throw new IllegalArgumentException("One or more of the arguments is not valid!");
        }
        return i;
    }

    // getters & setters
    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getPublisher()
    {
        return publisher;
    }

    public void setPublisher(String publisher)
    {
        this.publisher = publisher;
    }

    public String getAuthor()
    {
        return author;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public double getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public String toString()
    {
        return "Title is: " + this.title + "\nPublisher is: " + this.publisher + "\nAuthor is: " + this.author
                + "\nPrice is: $" + this.price + "\n";

    }

    public void setSortingMethod(int set)
    {
        sortingMethod = set;
    }

    // various types for compareTo (title, publisher, author alphabetical order;
    // price ascending order)
    @Override
    public int compareTo(Book o)
    {

        if (sortingMethod == 1)
        {
            if (this.title.compareTo(o.title) < 0)
            {
                return 1;
            }
        } else if (sortingMethod == 2)
        {
            if (this.publisher.compareTo(o.publisher) < 0)
            {
                return 1;
            }
        } else if (sortingMethod == 3)
        {
            if (this.author.compareTo(o.author) < 0)
            {
                return 1;
            }
        } else
        {
            if (this.price < o.price)
            {
                return 1;
            }
        }

        return 0;
    }

}
