import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * 1. Create a GUI with a canvas, this canvas implements a mouseactionlistener to gather points on the screen.
 * Create a draw button to connect these points.
 */
public class GUI_One extends JFrame implements ActionListener
{
    // class variables
    private boolean drawBoo = false, complete = false;
    private final JButton draw = new JButton("Draw");
    private final ClickCanvas canvas = new ClickCanvas();

    // no argument constructor
    public GUI_One()
    {
        this.setName("Drawing");
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.setSize(500, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * Initializes GUI
     */
    public void init()
    {
        draw.addActionListener(this);
        this.add(draw);
        draw.setSize(50, 20);
        this.add(canvas);
        this.setVisible(true);
    }

    /**
     * checks if draw button is clicked
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        // only draws if greater than 1 points exist
        if (canvas.points.size() > 1)
        {
            drawBoo = true;
        }
        canvas.repaint();
    }

    /**
     * canvas to draw points/line on
     */
    class ClickCanvas extends JPanel
    {
        // ArrayList of Points
        private final ArrayList<GUI_One.Point> points = new ArrayList<>();

        // no argument constructor
        public ClickCanvas()
        {
            this.setBackground(Color.BLUE);
            this.addMouseListener(
                    new MouseAdapter()
                    {
                        @Override
                        public void mouseReleased(MouseEvent e)
                        {
                            if (!complete)
                            {
                                super.mouseReleased(e);
                                points.add(new Point(e.getX(), e.getY()));
                                repaint();//calls paintComponent
                            }
                        }
                    }
            );
        }

        /**
         * if draw is clicked will draw a line between all Points in ArrayList
         * Adds new point to ArrayList and draws an oval at each point
         */
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);

            if (drawBoo) // if draw is clicked
            {
                // iterates through points and draws line from each one to next one
                for (int i = 0; i < points.size() - 1; i++)
                {
                    g.drawLine(points.get(i).x, points.get(i).y, points.get(i + 1).x, points.get(i + 1).y);
                }
                // draws line from first point to last point
                g.drawLine(points.get(0).x, points.get(0).y, points.get(points.size() - 1).x, points.get(points.size() - 1).y);
                complete = true;
            }


            // draws an oval at each point
            for (GUI_One.Point p : points)
            {
                g.setColor(Color.RED);
                g.fillOval(p.x - 5, p.y - 5, 10, 10);
            }

        }

    }

    /**
     * Class to track coordinated clicked
     */
    static class Point
    {
        int x, y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args)
    {
        new GUI_One().init();
    }
}
