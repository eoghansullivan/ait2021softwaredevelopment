public class Lab_1_reuse
{
    /*
     * public static void main(String[] args) { /* 1. Create an application which
     * will read a word from the command line, using a method(s) will analyse the
     * string and state its length, number of vowels, first letter and last letter.
     */

    // please feed this a command line argument, it was listed as a requirement
    /*
     * if (args[0] != null) { String s = args[0];
     * System.out.println(detailsToString(s)); System.out.println(); }
     */
    /*
     * // 2. A class Box complete with getter,setter and constructor methods, it
     * should // have dimensions, name, status(full/empty)
     * System.out.println("Boxs: "); Box b1 = new Box(); Box b2 = new Box(2, 3, 5,
     * "SuperBox", false); System.out.println(b1.toString()); System.out.println();
     * System.out.println(b2.toString()); System.out.println();
     *
     * /* 3. A class LightSwitch which has a status of on/off, it also has a random
     * chance to blow while it is being switched on. Use the input of plus to turn
     * on the light and minus to turn off the light. You must constantly poll the
     * keyboard for user input. The application ends when the light blows.
     *//*
 * System.out.println("Lightswitch:"); Scanner sc = new Scanner(System.in);
 * LightSwitch bulb = new LightSwitch(); while (!bulb.getBlown()) {
 * System.out.println("Please enter + to turn on light, - to turn off light");
 * bulb.switchOnOrOff(sc.next()); } sc.close(); }
 */

    /**
     * @param s String in Parses the string char by char and totals a vowel count to
     *          return
     * @return vowel count
     */
    static int numberOfVowels(String s)
    {
        int vow = 0;
        s = s.toLowerCase();
        for (char i : s.toCharArray())
        {
            if (i == 'a' || i == 'e' || i == 'i' || i == 'o' || i == 'u')
            {
                vow++;
            }
        }
        return vow;
    }

    /**
     * @param x Takes a string and prints the Length, number of vowels, first and
     *          last letters
     */
    static String detailsToString(String s)
    {
        return "String: " + s + "\nLength = " + s.length() + "\nNumber of Vowels: " + numberOfVowels(s)
                + "\nFirst letter: " + s.charAt(0) + "\nLast Letter: " + s.charAt(s.length() - 1);

    }

}
