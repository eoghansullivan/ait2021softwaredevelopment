import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Lab_3
{

    public static void main(String[] args) throws FileNotFoundException
    {
        // 1. Count the number of words in the text file (using java)
        File file = new File("files/Lab 3.txt");
        try
        {
            fileOperations(file);
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }

    /*
     * Takes a file, feeds it to a scanner, creates a String from the input,
     * separates words explicitly by a space, splits the string into an array of
     * strings (words) prints the amount of words, calls further methods to print
     * the number of vowels in the file and jumble the inner letters of words that
     * contain more than three letter
     */
    public static void fileOperations(File file) throws FileNotFoundException
    {
        String test = "";
        Scanner sc = new Scanner(file);

        // appends each next element to a String
        while (sc.hasNext())
        {
            test += sc.next() + " ";
        }

        // creates string array to count
        String[] arr = test.trim().split("\\s+");
        int count = arr.length;
        System.out.println("Word count is: " + count);

        /*
         * 2. Using previously developed methods count the number of vowels in the file
         * and the average number of vowels per word Averages and outputs vowels per
         * word
         */

        // outputs vowel count
        System.out.println("Vowel count is " + Lab_1_reuse.numberOfVowels(test));

        float out = (Lab_1_reuse.numberOfVowels(test) / (float) count);
        System.out.println("Average vowels per word is: " + out);
        sc.close();

        /*
         * 3. Print out the words in the file so that words longer than 3 chars will
         * have the first and last letter stay the same but the middle letters jumbled
         */

        // jumbles each element longer than 3 characters an outputs sequentially
        for (int i = 0; i < arr.length; i++)
        {
            arr[i] = removeNonAlphanumeric(arr[i]);
            arr[i] = jumbleLetters(arr[i]);
            System.out.println(String.valueOf(arr[i]));
        }
    }

    /**
     * @param str - replaces the string with an empty string except for the pattern
     *            "[^a-zA-Z0-9]"
     * @return str
     */
    static String removeNonAlphanumeric(String str)
    {
        str = str.replaceAll("[^a-zA-Z0-9]", "");
        return str;
    }

    /**
     * Takes String str and jumbles it if it is longer than three characters
     */
    static String jumbleLetters(String str)
    {

        if (str.length() > 3)
        {
            char[] arrange = str.toCharArray();
            for (int i = 1; i < str.length() - 2; i += 2)
            {
                char hold = arrange[i];
                arrange[i] = arrange[i + 1];
                arrange[i + 1] = hold;
            }
            str = String.valueOf(arrange);
        }

        return str;
    }

}
