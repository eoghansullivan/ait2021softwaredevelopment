import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class JavaServerClient
{


    public static void main(String[] args) throws Exception, InterruptedException
    {
        InetAddress inet = InetAddress.getByName("127.0.0.1");

        Socket s = new Socket(inet, 2003);

        Thread.sleep(200);

        OutputStream o = s.getOutputStream();
        PrintWriter pw = new PrintWriter(o);

        InputStream in = s.getInputStream();
        Scanner rx = new Scanner(in);

        Scanner myInput = new Scanner(System.in);

        String message = " ";

        String inputLine;

        do
        {
            System.out.print("Enter message for Server: ");
            message = myInput.nextLine();

            pw.print(message);
            pw.flush();
            inputLine = rx.nextLine();

            System.out.println("Message from Server: " + inputLine);

        } while (!message.equals("BYE"));
        pw.close();
        rx.close();

    }
}
