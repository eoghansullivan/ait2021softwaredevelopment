import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.LegendPosition;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.awt.*;
import java.util.Scanner;

public class ChartAssignment
{
    // class variable
    double[] inc;

    // constructor
    public ChartAssignment(double[] d)
    {
        inc = d;
    }

    public void createChart()
    {
        // arrays
        double[] xaxis = {1,2,3,4,5,6,7,8,9,10,11,12};
        double[] gamesArray = new double[]{25.50, 40, 45, 70, 10, 50, 45, 55, 20, 60, 65, 0};
        double[] socialArray = new double[]{0, 50, 140, 80, 350, 100, 125, 80, 70, 150, 100, 250};
        double[] incomeArray = inc;


        // initialization and styling
        XYChart chart = new XYChartBuilder().width(800).height(600).title("answer").xAxisTitle("Month").yAxisTitle("Amount Spent").build();
        chart.getStyler().setLegendPosition(LegendPosition.InsideNE);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);
        chart.getStyler().setAxisTicksMarksVisible(true);
        chart.getStyler().setAxisTickMarksColor(Color.BLUE);

        // array 1 init and style
        XYSeries details = chart.addSeries("Computer Games", xaxis, gamesArray);
        details.setMarker(SeriesMarkers.SQUARE);
        details.setMarkerColor(Color.BLUE);
        details.setLineColor(Color.BLUE);

        // array 2 init and style
        XYSeries details2 =chart.addSeries("videos", xaxis,socialArray);
        details2.setMarker(SeriesMarkers.CROSS);
        details2.setMarkerColor(Color.GREEN);
        details2.setLineColor(Color.GREEN);

        // array 3 init and style
        XYSeries details3 =chart.addSeries("cds", xaxis, incomeArray);
        details3.setMarker(SeriesMarkers.DIAMOND);
        details3.setMarkerColor(Color.ORANGE);
        details3.setLineColor(Color.ORANGE);

        new SwingWrapper(chart).displayChart();

    }
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        double[] income = new double[12];
        for (int i = 0; i < 12; i++)
        {
            System.out.println("Enter value " + (i+1));
            income[i] = sc.nextDouble();
        }


        ChartAssignment c = new ChartAssignment(income);
        c.createChart();
    }
}
