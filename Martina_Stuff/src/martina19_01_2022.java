public class martina19_01_2022
{
    public static void main(String[] args)
    {
        Person[] PersonArray = new Person[2];
        PersonArray[0] = new Person("John", 56);
        PersonArray[1] = new Person("Tom", 42);
        Person p1 = new Person("Jim", 45);
        PersonArray[1] = p1;
        p1.setName("Joseph");

        System.out.println(PersonArray[1]);
    }

}

class Person
{
    private String name, address;
    private int age;

    public Person(String name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public Person(String address, String name, int age)
    {
        this.address = address;
        this.name = name;
        this.age = age;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    @Override
    public String toString()
    {
        return "Person{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", age=" + age +
                '}';
    }
}
