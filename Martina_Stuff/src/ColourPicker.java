import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ColourPicker {

    public static void main(String[] args) {
        InetAddress inet;
        Scanner input = new Scanner(System.in);
        int choice = 10;
        String colour = "orange";
        try {
            inet = InetAddress.getByName("127.0.0.1");
            Socket s = new Socket(inet, 2003);
            Thread.sleep(200);
            OutputStream o = s.getOutputStream();

            PrintWriter pw = new PrintWriter(o);

            while(choice > 0) {
                System.out.println("Press 1 for red\nPress 2 for blue\nPress 3 for green\nPress 0 to exit");
                choice = input.nextInt();
                if(choice == 1) {
                    colour = "red";
                    System.out.println("you chose red");
                    pw.print(colour);
                    pw.flush();
                    Thread.sleep(200);
                }else if(choice == 2) {
                    colour = "blue";
                    System.out.println("you chose blue");
                    pw.print(colour);
                    pw.flush();
                    Thread.sleep(200);
                }else if (choice ==3) {
                    colour = "green";
                    System.out.println("you chose green");
                    pw.print(colour);
                    pw.flush();
                    Thread.sleep(200);
                }else if(choice==0){
                    System.out.println("thank you, program closing");

                }else {
                    System.out.println("sorry, try again, that number is not recognised");

                    Thread.sleep(200);
                }

            }
            pw.print("close");
            pw.flush();
            pw.close();
            o.close();
            System.exit(0);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}