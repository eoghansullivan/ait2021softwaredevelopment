import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class JavaToPython
{
    public static void main(String[] args)
    {

        InetAddress inet;
        String colour = "";

        try
        {
            inet = InetAddress.getByName("169.254.65.237");
            Socket s = new Socket(inet, 2003);
            Thread.sleep(200);
            OutputStream o = s.getOutputStream();

            PrintWriter pw = new PrintWriter(o);


            Scanner sc = new Scanner(System.in);
            while (true)
            {
                System.out.println("Press 1 for red");
                System.out.println("Press 2 for blue");
                System.out.println("Press 3 for green");
                System.out.println("Press 0 to exit");
                int selection = sc.nextInt();
                if (selection == 1)
                {
                    colour = "red";
                } else if (selection == 2)
                {
                    colour = "blue";
                } else if (selection == 3)
                {
                    colour = "green";
                } else if (selection == 0)
                {
                    System.out.println("thank you, program closing.");
                    pw.print("close");
                    pw.flush();
                    pw.close();
                    o.close();
                    System.exit(0);
                }else {
                    System.out.println("Wrong number, try again");
                    Thread.sleep(200);
                    continue;
                }

                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
