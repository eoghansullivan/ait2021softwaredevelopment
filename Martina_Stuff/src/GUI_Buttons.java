import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI_Buttons extends JFrame implements ActionListener
{
    // class variables
    private Dimension butts = new Dimension(150, 150);
    private JButton button1 = new JButton("Button left");
    private JButton button2 = new JButton("Button middle");
    private JButton button3 = new JButton("Button right");
    private JLabel textF = new JLabel("");
    private JCheckBox cb = new JCheckBox("Click me", false);
    private JLabel label2 = new JLabel("false");

    // no argument constructor
    public GUI_Buttons()
    {
        this.setTitle("Buttons GUI Test");
        this.setLayout(new GridLayout(2,3));
        this.setSize(1000, 500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    // initialize components
    public void init()
    {
        // set button sizes
        button1.setPreferredSize(butts);
        button2.setPreferredSize(butts);
        button3.setPreferredSize(butts);

        // add components to this frame
        this.add(button1);
        this.add(button2);
        this.add(button3);
        this.add(textF);
        this.add(label2);
        this.add(cb);

        // adding action listeners
        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        cb.addActionListener(this);

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == button1)
        {
            textF.setText("Left pressed");
            button1.setBackground(Color.RED);
            repaint();
        } else if (e.getSource() == button2)
        {
            textF.setText("Middle pressed");
            button2.setBackground(Color.BLUE);
            repaint();
        } else if (e.getSource() == button3)
        {
            textF.setText("Right pressed");
            button3.setBackground(Color.GREEN);
            repaint();
        } else if (e.getSource() == cb)
        {
            label2.setText(Boolean.toString(cb.isSelected()));
            button1.setBackground(Color.WHITE);
            button2.setBackground(Color.WHITE);
            button3.setBackground(Color.WHITE);
        }
    }

    public static void main(String[] args)
    {
        new GUI_Buttons().init();
    }
}
