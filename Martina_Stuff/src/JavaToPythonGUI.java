import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class JavaToPythonGUI extends JFrame implements ActionListener
{
    private JPanel myPanel = new JPanel();
    private JButton redButt = new JButton("Red"), blueButt = new JButton("Blue"),
            greenButt = new JButton("Green"), orangeButt = new JButton("Orange"),
            yellowButt = new JButton("Yellow"), pinkButt = new JButton("Pink"),
            exitButt = new JButton("Exit");

    private InetAddress inet;
    private String colour = "";
    private OutputStream o;
    private PrintWriter pw;
    private Socket s;

    public JavaToPythonGUI()
    {
        this.setTitle("Java to Python GUI");
        this.setMinimumSize(new Dimension(700, 500));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.add(myPanel);

        redButt.addActionListener(this);
        blueButt.addActionListener(this);
        greenButt.addActionListener(this);
        orangeButt.addActionListener(this);
        yellowButt.addActionListener(this);
        pinkButt.addActionListener(this);
        exitButt.addActionListener(this);

        myPanel.setLayout(new GridLayout(4, 3));
        myPanel.add(redButt);
        myPanel.add(blueButt);
        myPanel.add(greenButt);
        myPanel.add(orangeButt);
        myPanel.add(yellowButt);
        myPanel.add(pinkButt);
        myPanel.add(exitButt);

        this.setVisible(true);
        doServerStuff();
    }

    public void doServerStuff()
    {
        try
        {
            inet = InetAddress.getByName("169.254.65.237");
            s = new Socket(inet, 2003);
            Thread.sleep(200);
            o = s.getOutputStream();
            pw = new PrintWriter(o);

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try
        {
            if (e.getSource() == redButt)
            {
                System.out.println("red selected");
                colour = "red";
                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            } else if (e.getSource() == blueButt)
            {
                System.out.println("blue selected");
                colour = "blue";
                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            } else if (e.getSource() == greenButt)
            {
                System.out.println("green selected");
                colour = "green";
                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            } else if (e.getSource() == orangeButt)
            {
                System.out.println("orange selected");
                colour = "orange";
                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            } else if (e.getSource() == yellowButt)
            {
                System.out.println("yellow selected");
                colour = "yellow";
                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            } else if (e.getSource() == pinkButt)
            {
                System.out.println("pink selected");
                colour = "pink";
                pw.print(colour);
                pw.flush();
                Thread.sleep(200);
            } else if (e.getSource() == exitButt)
            {
                System.out.println("thank you, program closing.");
                pw.flush();
                pw.print("close");
                pw.flush();
                pw.close();
                o.close();
                System.exit(0);
            }
        } catch (InterruptedException | IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        new JavaToPythonGUI();
    }
}
