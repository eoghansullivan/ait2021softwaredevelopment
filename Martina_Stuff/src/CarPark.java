import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class CarPark

{
    private int index_row = 0, index_column = 0;
    private int avail = 800;
    private boolean[][] park = new boolean[40][20];
    private String[][] names = new String[40][20];

    public CarPark()
    {
        populate();
        book();
    }


    /**
     * populates boolean&String Arrays with falses
     */
    private void populate()
    {
        for (int i = 0; i < 40; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                park[i][j] = false;
                names[i][j] = "Empty";
            }
        }
    }

    public static void main(String[] args)
    {
        new CarPark();
    }

    public void book()
    {
        Scanner sc = new Scanner(System.in);
        while (avail > 1)
        {
            System.out.println("Do you want to book a spot y/n?");

            if (sc.next().equalsIgnoreCase("y"))
            {
                System.out.println("There are " + avail + " spots left");
                System.out.println("Book a spot?");
                if (sc.next().equalsIgnoreCase("y"))
                {
                    if (index_column > 19)
                    {
                        index_column = 0;
                        index_row++;
                    }
                    System.out.println("Whats your name?");
                    names[index_row][index_column] = sc.next();
                    park[index_row][index_column] = true;
                    avail--;
                    print();
                } else
                {
                    System.out.println("bye");
                    print();
                    System.exit(0);
                }


            } else
            {
                System.out.println("bye");
                print();
                System.exit(0);
            }
        }
        System.out.println("No spots left");
        print();
    }

    /**
     * prints both (name and availability) arrays
     */
    public void print()
    {
        System.out.println("Names:");
        System.out.println(Arrays.deepToString(names));
        System.out.println("Taken:");
        System.out.println(Arrays.deepToString(park));
    }
}
