import org.knowm.xchart.*;
import java.awt.*;
import java.net.InetAddress;
import java.net.Socket;
import java.io.*;
import java.util.Scanner;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;
import org.knowm.xchart.style.Styler.LegendPosition;

import javax.swing.*;

public class Assignment_Part_1 extends JFrame
{
    double[] tempsArray = new double[0];

    public Assignment_Part_1()
    {
        createChart();
    }

    public static void main(String[] args)
    {
        new Assignment_Part_1();
    }

    public void createChart()
    {
        // arrays

        try
        {
            tempsArray = getTheTempsArray();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        double[] xaxis = new double[tempsArray.length];
        for (int i = 0; i < tempsArray.length; i++)
        {
            xaxis[i] = i + 1;
        }

        // initialization and styling
        XYChart chart = new XYChartBuilder().width(800).height(600).title("Temperatures").xAxisTitle("times").yAxisTitle("values").build();
        chart.getStyler().setLegendPosition(LegendPosition.OutsideE);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);
        chart.getStyler().setAxisTicksMarksVisible(true);
        chart.getStyler().setAxisTickMarksColor(Color.BLUE);

        double max = getMax(tempsArray);
        double min = getMin(tempsArray);
        double avg = getAverage(tempsArray);

        // set up chart
        XYSeries details = chart.addSeries("degrees celcius", xaxis, tempsArray);
        details.setLineColor(Color.BLUE);
        JLabel maxLabel = new JLabel("Max =" + max);
        JLabel minLabel = new JLabel("Min = " + min);
        JLabel avgLabel = new JLabel("Average = " + avg);
        JLabel detailsLabel = new JLabel("Max: " + max + " Min: " + min + " Average: " + avg);
        JPanel detailsPanel = new JPanel();
        XChartPanel pan = new XChartPanel(chart);

        // set up GUI
        this.setLayout(new FlowLayout());
        this.add(pan);
        this.add(detailsPanel);
        detailsPanel.add(detailsLabel);
        this.setSize(new Dimension(900, 500));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

    /**
     * communicates with python server and parses received string into double array
     */
    public double[] getTheTempsArray() throws InterruptedException, IOException
    {
        // opens a socket on a particular ip address
        InetAddress inet = InetAddress.getByName("127.0.0.1");
        Socket s = new Socket(inet, 2003);
        Thread.sleep(200);

        // takes input from python program and feeds to scanner
        InputStream in = s.getInputStream();
        Scanner sc = new Scanner(in);

        // creates a string array based on the string input split on comma separation
        String inputLine = sc.nextLine();
        inputLine = inputLine.replaceAll("[a-zA-Z]", "");
        System.out.println(inputLine);
        String[] parsed = inputLine.split(",");

        // double to populate
        double[] parsedD = new double[parsed.length];

        // removes commas from string array and parses the contents into a Double array
        for (int i = 0; i < parsed.length; i++)
        {
            parsed[i] = parsed[i].replaceAll(",", "");
            parsedD[i] = Double.parseDouble(parsed[i]);
        }

        sc.close();
        s.close();

        return parsedD;
    }


    /**
     * returns max value from a double array
     */
    public double getMax(double[] d)
    {
        double max = d[0];
        for (int i = 0; i < d.length; i++)
        {
            if (d[i] > max)
            {
                max = d[i];
            }
        }
        return max;
    }

    /**
     * returns min value from a double array
     */
    public double getMin(double[] d)
    {
        double min = d[0];
        for (int i = 0; i < d.length; i++)
        {
            if (d[i] < min)
            {
                min = d[i];
            }
        }
        return min;
    }

    /**
     * returns the average value from a double array
     */
    public double getAverage(double[] d)
    {
        double tot = 0d;
        for (int i = 0; i < d.length; i++)
        {
            tot += d[i];
        }
        return tot / d.length;
    }
}
