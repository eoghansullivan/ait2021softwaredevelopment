import javax.sound.sampled.*;
import javax.swing.*;
import java.io.File;
import java.io.IOException;

/**
 * extends part one and adds audio playing on < 0 and > 20 degrees
 */
public class Assignment_Part_2 extends Assignment_Part_1
{
    // class variables
    DataLine.Info info;
    DataLine.Info info2;
    AudioInputStream stream;
    AudioInputStream stream2;
    JLabel warning = new JLabel("");

    public static void main(String[] args)
    {
        new Assignment_Part_2();
    }

    public Assignment_Part_2()
    {
        super();
        try
        {
            // initializes audio
            initAudioStreams();
            initAudioStreams2();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // plays alerts under circumstances
        for (int i = 0; i < tempsArray.length; i++)
        {
            if (tempsArray[i] > 20)
            {
                try
                {
                    playOverTwentyAlert(tempsArray[i]);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            if (tempsArray[i] < 0)
            {
                try
                {
                    playUnderZeroAlert(tempsArray[i]);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
        this.add(warning);
        this.pack();

    }

    /**
     * Initialize audio stream
     */
    public void initAudioStreams() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));
        stream = AudioSystem.getAudioInputStream(new File("endAssignmentMartina/src/over20.wav"));


        AudioFormat format = stream.getFormat();
        if (format.getEncoding() != AudioFormat.Encoding.PCM_SIGNED)
        {
            format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, format.getSampleRate(),
                    format.getSampleSizeInBits() * 2, format.getChannels(), format.getFrameSize() * 2,
                    format.getFrameRate(), true); // big endian
            stream = AudioSystem.getAudioInputStream(format, stream);
        }

        info = new DataLine.Info(Clip.class, stream.getFormat(),
                ((int) stream.getFrameLength() * format.getFrameSize()));
    }

    /**
     * Initialize audio stream 2
     */
    public void initAudioStreams2() throws Exception
    {
        System.out.println(System.getProperty("user.dir"));
        stream2 = AudioSystem.getAudioInputStream(new File("endAssignmentMartina/src/under20.wav"));


        AudioFormat format = stream2.getFormat();
        if (format.getEncoding() != AudioFormat.Encoding.PCM_SIGNED)
        {
            format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, format.getSampleRate(),
                    format.getSampleSizeInBits() * 2, format.getChannels(), format.getFrameSize() * 2,
                    format.getFrameRate(), true); // big endian
            stream2 = AudioSystem.getAudioInputStream(format, stream2);
        }

        info2 = new DataLine.Info(Clip.class, stream2.getFormat(),
                ((int) stream2.getFrameLength() * format.getFrameSize()));
    }

    /**
     * Play alert and update warning label on GUI
     */
    public void playOverTwentyAlert(double temp) throws InterruptedException
    {
        Clip clip = null;
        try
        {
            clip = (Clip) AudioSystem.getLine(info);
        } catch (LineUnavailableException e)
        {
            e.printStackTrace();
        }

        try
        {
            clip.open(stream);
        } catch (LineUnavailableException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        clip.start();
        warning.setText(warning.getText() + " " + temp + " is over 20, playing alert.  ");
    }

    /**
     * Play alert 2 and update warning label on GUI
     */
    public void playUnderZeroAlert(double temp) throws InterruptedException
    {
        Clip clip = null;
        try
        {
            clip = (Clip) AudioSystem.getLine(info2);
        } catch (LineUnavailableException e)
        {
            e.printStackTrace();
        }

        try
        {
            clip.open(stream2);
        } catch (LineUnavailableException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        clip.start();
        warning.setText(warning.getText() + " " + temp + " is under, playing alert.");
    }

}
