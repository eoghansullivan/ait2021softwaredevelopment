import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ReadCSV
{

    private ArrayList<LocalEleStat> stats = new ArrayList<>();
    private String[] headings;
    private String[] uniqueConsts;
    private ArrayList<String> uniqueConstituency = new ArrayList<>();
    private String areaCandidates;
    String parsedStringStat = "";

    private String toAppend = "";


    public ReadCSV()
    {
        try
        {
            Scanner sc = new Scanner(new File("input2.csv"));

            sc.nextLine();//skip first heading
            headings = sc.nextLine().split(",");//add second row to headings

            while (sc.hasNextLine())
            {
                try
                {
                    String s = sc.nextLine();
                    // System.out.println(s);
                    stats.add(new LocalEleStat(s));
                } catch (IllegalArgumentException ex)
                {
                    // do nothing
                }
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void addNewStat(String s)
    {
        toAppend += s + "\n";
        stats.add(new LocalEleStat(s));
    }


    public String[] updateAndReturnConstituencies()
    {
        for (LocalEleStat st : stats)
        {
            if (!uniqueConstituency.contains(st.getLocalElectoralArea()))
            {
                uniqueConstituency.add(st.getLocalElectoralArea());
            }
        }
        uniqueConsts = uniqueConstituency.toArray(new String[0]);
        return uniqueConsts;
    }

    public String getCandidatesFromArea(int i)
    {
        areaCandidates = "<html>";
        for (LocalEleStat st : stats)
        {
            if (uniqueConsts[i].equalsIgnoreCase(st.getLocalElectoralArea()))
            {
                areaCandidates += st.getFirstName() + " " + st.getSurname();
                areaCandidates += "<br>";
            }
        }
        areaCandidates += "</html>";
        return areaCandidates;

    }

    public String[] getCandidateNamesList()
    {
        String[] nameList = new String[stats.size()];
        for (int i = 0; i < stats.size(); i++)
        {
            nameList[i] = stats.get(i).getFirstName() + " " + stats.get(i).getSurname();
        }
        return nameList;
    }

    public void saveFile()
    {
        try
        {
            Writer output;
            output = new BufferedWriter(new FileWriter("input2.csv", true));
            output.append(toAppend);
            toAppend = "";
            output.close();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }
    }

    public void deleteCandidate(String s)
    {
        String fName = s.split(" ")[0];
        String surName = s.split(" ")[1];




        for (int i = 0; i < stats.size(); i ++)
        {
            if (stats.get(i).getSurname().equalsIgnoreCase(surName) && stats.get(i).getFirstName().equalsIgnoreCase(fName))
            {
                stats.remove(stats.get(i));
                System.out.println("Person deleted");
                break;
            }else
            {
                System.out.println("Person not found");
            }
        }
/*
        for (LocalEleStat st : stats)
        {
            parsedStringStat = "";
            parsedStringStat += "x" + ",";
            parsedStringStat += st.getSurname().replaceAll(",", "") + ",";
            parsedStringStat += st.getFirstName().replaceAll(",", "") + ",";
            parsedStringStat += "\"" + st.getAddress().toString().replaceAll("\"", "") + "\",";
            parsedStringStat += st.getParty().replaceAll(",", "") + ",";
            parsedStringStat += st.getLocalElectoralArea().replaceAll(",", "") + ",,,,,";
        }

        try
        {
            Writer output;
            output = new BufferedWriter(new FileWriter("input2.csv", true));  //clears file every time
            output.append(toAppend);
            toAppend = "";
            output.close();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }

         */
    }

    public ArrayList<LocalEleStat> getStats()
    {
        return stats;
    }

    public String[] getHeadings()
    {
        return headings;
    }

    public static void main(String[] args)
    {
        System.out.println(new ReadCSV().getStats());
    }
}
