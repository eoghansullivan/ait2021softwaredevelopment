import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUIPart extends JFrame implements ActionListener, ChangeListener
{

    private JTabbedPane tabbedPane = new JTabbedPane();
    private int no = 1;

    private JTable table;

    private JScrollPane scrollPane;

    private JPanel p1 = new JPanel();
    private JPanel p2 = new JPanel();
    private JPanel p3 = new JPanel();

    private JTextField surNameInput = new JTextField(9);
    private JTextField firstNameInput = new JTextField(8);
    private JTextField partyInput = new JTextField(8);
    private JTextField areaInput = new JTextField(10);
    private JTextField addressInput = new JTextField(20);

    private JLabel lastNameLabel = new JLabel("Surname");
    private JLabel firstNameLabel = new JLabel("First name");
    private JLabel partyLabel = new JLabel("Party");
    private JLabel areaLabel = new JLabel("Local Electoral Area");
    private JLabel addressLabel = new JLabel("Address");

    private JButton addCandidate = new JButton("Add");
    private JButton delButt = new JButton("Delete");
    private JButton saveCSV = new JButton("Save CSV FILE");
    private GridBagConstraints gbc = new GridBagConstraints();
    private String[] cols;
    private DefaultTableModel model;

    private JComboBox<String> candidateListForDelete;

    // ----------- third page (combobox)
    String[] uniqueVotingAreas;
    private JComboBox<String> constituenciesCombos;
    private JLabel listOfCandidates = new JLabel();
    private ReadCSV csv;


    public GUIPart()
    {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle("GUI Part");
        this.setSize(800, 750);
        this.setMinimumSize(new Dimension(800, 750));
    }


    public void init()
    {
        addCandidate.addActionListener(this);
        saveCSV.addActionListener(this);
        tabbedPane.addChangeListener(this);

        csv = new ReadCSV();
        cols = csv.getHeadings();
        uniqueVotingAreas = csv.updateAndReturnConstituencies();
        updatePanelThree();
        updateTable();
        // ------------ panel two items


        p1.setLayout(new GridBagLayout());


        panelOneGBC();

        tabbedPane.add("First Tab", p1);
        tabbedPane.add("Second Tab", p2);
        tabbedPane.add("Third Tab", p3);

        this.add(tabbedPane);

        this.setVisible(true);

       // csv.deleteCandidate();

    }


    public static void main(String[] args)
    {
        new GUIPart().init();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == constituenciesCombos)
        {
            listOfCandidates.setText(csv.getCandidatesFromArea(constituenciesCombos.getSelectedIndex()));
        }
        if (e.getSource() == addCandidate)
        {
            String toAdd = "";
            toAdd += no + ",";
            toAdd += surNameInput.getText().replaceAll(",", "") + ",";
            toAdd += firstNameInput.getText().replaceAll(",", "") + ",";
            toAdd += "\"" + addressInput.getText().replaceAll("\"", "") + "\",";
            toAdd += partyInput.getText().replaceAll(",", "") + ",";
            toAdd += areaInput.getText().replaceAll(",", "") + ",,,,,";
            csv.addNewStat(toAdd);
            System.out.println(toAdd);
            p2.remove(scrollPane);
            System.out.println("Added candidate");
            updateTable();
        }
        if (e.getSource() == saveCSV)
        {
            csv.saveFile();
        }
        if(e.getSource() == delButt)
        {
            csv.deleteCandidate(candidateListForDelete.getSelectedItem().toString());
        }
    }


    public void panelOneGBC()
    {
        delButt.addActionListener(this);
        p1.add(firstNameInput, gbc);

        gbc.gridx = 1;
        p1.add(surNameInput, gbc);

        gbc.gridx = 2;
        p1.add(partyInput, gbc);

        gbc.gridx = 3;
        p1.add(areaInput, gbc);

        gbc.gridx = 4;
        p1.add(addressInput, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        p1.add(firstNameLabel, gbc);

        gbc.gridx = 1;
        p1.add(lastNameLabel, gbc);

        gbc.gridx = 2;
        p1.add(partyLabel, gbc);

        gbc.gridx = 3;
        p1.add(areaLabel, gbc);

        gbc.gridx = 4;
        p1.add(addressLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        p1.add(addCandidate, gbc);
        gbc.gridx = 1;
        p1.add(saveCSV, gbc);
        gbc.gridx = 0;
        gbc.gridy = 3;
        candidateListForDelete = new JComboBox<>(csv.getCandidateNamesList());
        candidateListForDelete.addActionListener(this);
        p1.add(candidateListForDelete, gbc);
        gbc.gridx = 1;
        p1.add(delButt, gbc);

    }

    public void updateTable()
    {
        no = 1;
        model = new DefaultTableModel(cols, 0);
        for (LocalEleStat stat : csv.getStats())
        {
            model.addRow(new Object[]{
                    no,
                    stat.getSurname(),
                    stat.getFirstName(),
                    stat.getAddress(),
                    stat.getParty(),
                    stat.getLocalElectoralArea()
            });
            no++;
        }
        table = new JTable(model);
        table.setAutoCreateRowSorter(true);
        scrollPane = new JScrollPane(table);

        p2.add(scrollPane);
    }

    public void updatePanelThree()
    {
        uniqueVotingAreas = csv.updateAndReturnConstituencies();
        constituenciesCombos = new JComboBox<>(uniqueVotingAreas);
        constituenciesCombos.addActionListener(this);
        p3.add(constituenciesCombos);
        listOfCandidates.setText(csv.getCandidatesFromArea(constituenciesCombos.getSelectedIndex()));
        p3.add(listOfCandidates);
    }

    @Override
    public void stateChanged(ChangeEvent e)
    {
        p3.remove(listOfCandidates);
        p3.remove(constituenciesCombos);
        updatePanelThree();
    }
}
