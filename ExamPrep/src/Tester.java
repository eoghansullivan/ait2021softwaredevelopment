import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Tester
{
    public static void main(String[] args)
    {
        try
        {
            Writer output;
            output = new BufferedWriter(new FileWriter("test.txt", true));  //clears file every time
            output.append("\nNew Line!\nPooLine");
            output.close();
        } catch (
                IOException e)

        {
            e.printStackTrace();
        }
    }

}
