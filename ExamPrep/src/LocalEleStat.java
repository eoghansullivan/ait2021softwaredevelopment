import java.util.Arrays;
import java.util.Scanner;

public class LocalEleStat {

    private String surname,firstName,party,localElectoralArea;
    private Address address;

    public LocalEleStat(String s)
    {
        try {
            //System.out.println(s);
            Scanner sc = new Scanner(s);

            sc.useDelimiter(",");
            sc.next();
            surname = sc.next();
            firstName = sc.next();

            sc.useDelimiter("\"");
            sc.next();
            address = new Address(sc.next());

            sc.useDelimiter(",");
            sc.next();
            party = sc.next();
            localElectoralArea = sc.next();
        }
        catch (Exception e)
        {
            //ignore exceptions
            throw new IllegalArgumentException("doesnt fit");
        }
    }

    @Override
    public String toString() {
        return "LocalEleStat{" +
                "surname='" + surname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", party='" + party + '\'' +
                ", localElectoralArea='" + localElectoralArea + '\'' +
                ", address=" + address +
                '}';
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getParty() {
        return party;
    }

    public String getLocalElectoralArea() {
        return localElectoralArea;
    }

    public Address getAddress() {
        return address;
    }

    class Address
    {
        String [] lines;

        public Address(String s)
        {
            lines = s.split(",");
        }


        @Override
        public String toString() {
            return  Arrays.toString(lines);

        }
    }
}
