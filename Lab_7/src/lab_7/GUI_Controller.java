package lab_7;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI_Controller
{
    //declares view and model objects
    private final GUI_View view;
    private final MultipleCurrencyConverter model;

    //constructor
    public GUI_Controller(GUI_View view, MultipleCurrencyConverter model)
    {
        this.view = view;
        this.model = model;
        new USDController();
        new EuroController();
        new GBPController();
        new YUANController();
        view.init();
    }


    /**
     * gets the value of USD from the view, saves to model
     * sets the converted values in the view
     */
    public void setUSD()
    {
        if (!view.getUsdText().isEmpty())
        {
            //get value from usd text field
            model.setUsd_value(Double.parseDouble(view.getUsdText()));

            setView();

        }
    }

    /**
     * gets the value of Euro from the view, converts to USD
     * calculates conversions, sets the converted values in the view
     */
    public void setEur()
    {
        if (!view.getEuroText().isEmpty())
        {
            model.eurToUSD(Double.parseDouble(view.getEuroText()));
            setView();
        }
    }

    /**
     * gets the value of GBP from the view, converts to USD
     * calculates conversions, sets the converted values in the view
     */
    public void setGBP()
    {
        if (!view.getGbpText().isEmpty())
        {
            model.gbpToUSD(Double.parseDouble(view.getGbpText()));
            setView();
        }
    }

    /**
     * gets the value of Yuan from the view, converts to USD
     * calculates conversions, sets the converted values in the view
     */
    public void setYUAN()
    {
        if (!view.getYuanText().isEmpty())
        {
            model.yuanToUSD(Double.parseDouble(view.getYuanText()));
            setView();
        }
    }

    /**
     * calculates the values in the model and update the view
     */
    public void setView()
    {
        // calculates & sets the GBP in view to the value from model method
        view.setGbpText(Double.toString(model.usdToGBP()));
        // calculates & sets the Euro in view to the value from model method
        view.setEuroText(Double.toString(model.usdToEur()));
        // calculates & sets the Yuan in view to the value from model method
        view.setYuanText(Double.toString(model.usdToYuan()));

        // sets the USD in view to the value from model variable (only necessary sometimes)
        view.setUsdText(Double.toString(model.getUsd_value()));
    }

    /*
    below are internal classes which act as Action Listeners for elements in the View
    passing themselves in as parameters and performing a single action each
    */

    class EuroController implements ActionListener
    {
        public EuroController()
        {
            view.addEuroListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            setEur();
        }
    }

    class USDController implements ActionListener
    {
        public USDController()
        {
            view.addUSDListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            setUSD();
        }
    }

    class GBPController implements ActionListener
    {
        public GBPController()
        {
            view.addGBPListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            setGBP();
        }
    }

    class YUANController implements ActionListener
    {
        public YUANController()
        {
            view.addYUANListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            setYUAN();
        }
    }
}
