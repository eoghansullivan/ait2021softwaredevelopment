package lab_7;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI_View extends JFrame
{
    // declaration and initialization of Text fields and labels
    private final JTextField usdText = new JTextField();
    private final JTextField euroText = new JTextField();
    private final JTextField gbpText = new JTextField();
    private final JTextField yuanText = new JTextField();
    private final JLabel usdLabel = new JLabel("USD", SwingConstants.CENTER);
    private final JLabel euroLabel = new JLabel("Euro", SwingConstants.CENTER);
    private final JLabel gbpLabel = new JLabel("GBP", SwingConstants.CENTER);
    private final JLabel yuanLabel = new JLabel("YUAN", SwingConstants.CENTER);

    // no parameter constructor to set up the JFrame
    public GUI_View()
    {
        this.setTitle("Currency Converter");
        this.setSize(550, 100);
        this.setLayout(new GridLayout(2, 4));
    }

    // initialization of buttons and text fields being added to Frame
    public void init()
    {
        this.add(usdText);
        this.add(euroText);
        this.add(gbpText);
        this.add(yuanText);

        this.add(usdLabel);
        this.add(euroLabel);
        this.add(gbpLabel);
        this.add(yuanLabel);

        this.setVisible(true);
    }

    // getters
    public String getUsdText()
    {
        return usdText.getText();
    }

    // setters
    public void setUsdText(String s)
    {
        usdText.setText(s);
    }

    public String getEuroText()
    {
        return euroText.getText();
    }

    public void setEuroText(String s)
    {
        euroText.setText(s);
    }

    public String getGbpText()
    {
        return gbpText.getText();
    }

    public void setGbpText(String s)
    {
        gbpText.setText(s);
    }

    public String getYuanText()
    {
        return yuanText.getText();
    }

    public void setYuanText(String s)
    {
        yuanText.setText(s);
    }

    // below are methods which take passed action listeners from the controller and assign them to text fields
    public void addEuroListener(ActionListener al1)
    {
        euroText.addActionListener(al1);
    }

    public void addUSDListener(ActionListener al2)
    {
        usdText.addActionListener(al2);
    }

    public void addGBPListener(ActionListener al3)
    {
        gbpText.addActionListener(al3);
    }

    public void addYUANListener(ActionListener al4)
    {
        yuanText.addActionListener(al4);
    }
}
