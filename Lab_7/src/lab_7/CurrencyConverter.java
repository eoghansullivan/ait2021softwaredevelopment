package lab_7;

public class CurrencyConverter implements BaseCurrency
{
    private double usdValue = 0d;

    //rounds
    public static double round(double value)
    {
        return Math.round(value * 100.) / 100.00;
    }

    /**
     * converts euro to usd
     */
    @Override
    public void eurToUSD(double eurIn)
    {
        usdValue = round(eurIn / 0.87);
    }

    /**
     * converts usd to euro
     */
    @Override
    public double usdToEur()
    {
        return round(usdValue * 0.87);
    }

    //getter and setter for usd
    @Override
    public double getUsd_value() {return round(usdValue);}

    @Override
    public void setUsd_value(double usd_value) {this.usdValue = round(usd_value);}

    //below are for subclass
    @Override
    public void gbpToUSD(double gbpIn) {}

    @Override
    public double usdToGBP() {return 0;}

    @Override
    public void yuanToUSD(double yuanIn) {}

    @Override
    public double usdToYuan() {return 0;}

}
