package lab_7;

public class Lab_7
{
    public static void main(String[] args)
    {
        partOne();
        MultipleCurrencyConverter mcc = new MultipleCurrencyConverter();
        partTwo(mcc);
        partThree(mcc);
    }

    /**
     * 1. Create a class CurrencyConverter which implements an Interface BaseCurrency(USD)
     * write a test class to convert to this base currency from Euro. (You can find current exchange rates on XE.com)
     */
    public static void partOne()
    {
        CurrencyConverter cc = new CurrencyConverter();

        cc.eurToUSD(50);
        System.out.println("50 Euro is " + cc.getUsd_value() + " USD");

        cc.setUsd_value(100);
        System.out.println(cc.getUsd_value() + " USD = " + cc.usdToEur() + " Euro");
    }

    /**
     * 2. Extend the CurrencyConverter class to become MultipleCurrencyConverter
     * modify the interface for the currencies Euro, Yuan, and GBP, create a test class for this.
     */
    public static void partTwo(MultipleCurrencyConverter mcc)
    {
        mcc.gbpToUSD(50);
        System.out.println("50 GBP is " + mcc.getUsd_value() + " USD");

        mcc.setUsd_value(100);
        System.out.println(mcc.getUsd_value() + " USD = " + mcc.usdToGBP() + " GBP");

        mcc.yuanToUSD(50);
        System.out.println("50 Yuan is " + mcc.getUsd_value() + " USD");

        mcc.setUsd_value(100);
        System.out.println(mcc.getUsd_value() + " USD = " + mcc.usdToYuan() + " Yuan");
    }

    /**
     * 3. Create a GUI which can update a display for all currencies based on the input of an amount in
     * any currency field
     */
    public static void partThree(MultipleCurrencyConverter mcc)
    {
        GUI_View view = new GUI_View();
        new GUI_Controller(view, mcc);
    }


}
