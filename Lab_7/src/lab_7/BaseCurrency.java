package lab_7;

public interface BaseCurrency
{
    //abstract methods
    void eurToUSD(double eurIn);

    double usdToEur();

    void gbpToUSD(double gbpIn);

    double usdToGBP();

    void yuanToUSD(double yuanIn);

    double usdToYuan();

    //getters & setters
    double getUsd_value();

    void setUsd_value(double usd_value);


}
