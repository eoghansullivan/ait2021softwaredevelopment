package lab_7;

public class MultipleCurrencyConverter extends CurrencyConverter
{
    /**
     * converts GBP to USD
     */
    @Override
    public void gbpToUSD(double gbpIn) {setUsd_value(round(gbpIn / 0.73));}

    /**
     * converts USD to GBP
     */
    @Override
    public double usdToGBP() {return round(super.getUsd_value() * 0.73);}

    /**
     * converts YUAN to USD
     */
    @Override
    public void yuanToUSD(double yuanIn) {super.setUsd_value(round(yuanIn / 6.35));}

    /**
     * converts USD to YUAN
     */
    @Override
    public double usdToYuan() {return round(super.getUsd_value() * 6.35);}

}
