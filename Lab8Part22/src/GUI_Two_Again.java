import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class GUI_Two_Again extends JFrame
{
    private DrawingPanel canvas = new DrawingPanel();
    private static Point clickCoord;

    public GUI_Two_Again()
    {
        this.setName("Shapes"); this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
        this.setSize(500, 500); this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void init()
    {
        this.add(canvas); this.setBackground(Color.PINK); this.setVisible(true);
    }

    public static void setClickCoord(Point p)
    {
        clickCoord = p;
    }

    public static Point getClickCoord()
    {
        return clickCoord;
    }

    class DrawingPanel extends JPanel
    {
        private static int shapeCount = 0;
        private ArrayList<MyRectangle> rectangles = new ArrayList<>();
        private ArrayList<MySphere> spheres = new ArrayList<>();
        private static int shapeType = 1; // 1 rectangle (more to be added), 2 circle //more to be added
        private static int shapeTypeClicked = 0; // 1 rec, 2 sph
        private static int clickedLastIndex = -2;
        private static int currentClicked = 0;
        private boolean changingBG = false;


        public DrawingPanel()
        {

            //this.setOpaque(true);
            this.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    super.mouseClicked(e);
                    GUI_Two_Again.setClickCoord(e.getPoint());

                    for (MyRectangle items : rectangles)
                    {
                        if (items.contains(e.getPoint()))
                        {
                            System.out.println("Changing bg");
                            changingBG = true;
                            currentClicked = items.getId();
                            System.out.println("Just clicked square : " + currentClicked);
                            shapeTypeClicked = 1;
                            repaint();

                        }
                    } //repaint();
                    for (MySphere items : spheres)
                    {
                        if (items.contains(e.getPoint()))
                        {
                            System.out.println("Changing bg");
                            changingBG = true;
                            currentClicked = items.getId();
                            System.out.println("Just clicked sphere : " + currentClicked);
                            shapeTypeClicked = 2;
                            repaint();

                        }
                    }
                    if (shapeTypeClicked == 1)
                    {
                        clickedLastIndex = spheres.get(spheres.size() - 1).sphereIndexID;
                    } else if (shapeTypeClicked == 2)
                    {
                        clickedLastIndex = rectangles.get(rectangles.size() - 1).rectangleIndexID;
                    }
                    repaint();

                }
            });
        }

        public static int getShapeTypeClicked()
        {
            return shapeTypeClicked;
        }

        public static void setShapeRandomType()
        {
            /*
            int pu = 1 + (int) (Math.random() * 2);
            System.out.println(pu + " is random shape");
            shapeType = pu;

             */
            if (shapeType == 1)
            {
                shapeType = 2;
            } else if (shapeType == 2)
            {
                shapeType = 1;
            }
        }

        public static void addShapeCount()
        {
            shapeCount++;
        }

        public static int getShapeCount()
        {
            return shapeCount;
        }

        public static int getShapeType()
        {
            return shapeType;
        }

        public void paintComponent(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;

            if (!changingBG)
            {
                if (clickedLastIndex >= -1)
                {
                    DrawingPanel.setShapeRandomType();

                    if (DrawingPanel.getShapeType() == 1)
                    {
                        rectangles.add(new MyRectangle());
                        if (clickedLastIndex == -1)
                        {
                            clickedLastIndex++;
                        } else
                        {
                            int len = rectangles.size(); clickedLastIndex = rectangles.get(len - 1).rectangleIndexID;
                        }
                        if (GUI_Two_Again.getClickCoord() != null)
                        {
                            rectangles.get(clickedLastIndex).setRect(GUI_Two_Again.getClickCoord().getX() - 25,
                                    GUI_Two_Again.getClickCoord().getY() - 25, 50, 50);
                            g2d.fill(rectangles.get(clickedLastIndex));
                        }
                    } else if (DrawingPanel.getShapeType() == 2)
                    {
                        spheres.add(new MySphere());
                        if (clickedLastIndex == -1)
                        {
                            clickedLastIndex++;
                        } else
                        {
                            int len = spheres.size(); clickedLastIndex = spheres.get(len - 1).sphereIndexID;
                        }
                        if (GUI_Two_Again.getClickCoord() != null)
                        {
                            spheres.get(clickedLastIndex).setFrame(GUI_Two_Again.getClickCoord().getX() - 25,
                                    GUI_Two_Again.getClickCoord().getY() - 25, 50, 50);
                            g2d.fill(spheres.get(clickedLastIndex));
                        }
                    }


                } else
                {
                    clickedLastIndex = -1;
                }
            } else

            {
                changeBackground(g2d, currentClicked, rectangles, shapeType, spheres);
                changingBG = false;
            }
        }
    }


    class MyRectangle extends Rectangle2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private static int rectangleCount = 0;
        private int rectangleIndexID;
        private int bgColour = 0;

        public MyRectangle()
        {
            DrawingPanel.addShapeCount();
            rectangleCount++;
            this.rectangleIndexID = rectangleCount - 1;
            System.out.println(this.rectangleIndexID + " rectangles");
        }

        public int getId()
        {
            return rectangleIndexID;
        }

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    class MySphere extends Ellipse2D.Double
    {
        // 0 black, 1 red, 2 green, 3 blue
        private static int sphereCount = 0;
        private int sphereIndexID;
        private int bgColour = 0;

        public MySphere()
        {
            DrawingPanel.addShapeCount();
            sphereCount++;
            this.sphereIndexID = DrawingPanel.getShapeCount() - 1;
            System.out.println(this.sphereIndexID + " spheres");
        }

        public int getId()
        {
            return sphereIndexID;
        }

        public int getBgColour()
        {
            return bgColour;
        }

        public void setBgColour(int col)
        {
            this.bgColour = col;
        }
    }

    public void changeBackground(Graphics2D g2d, int currentClicked, ArrayList<MyRectangle> rectangles, int typeOfShape, ArrayList<MySphere> spheres)
    {
        if (typeOfShape == 1)
        {
            System.out.println("Rectangle " + currentClicked + " was clicked last");
            int currentCol = rectangles.get(currentClicked).getBgColour();
            System.out.println("Current bg colour is " + currentCol);
            if (currentCol == 0)
            {
                g2d.setColor(Color.RED);
                rectangles.get(currentClicked).setBgColour(1);
                g2d.fill(rectangles.get(currentClicked));
            } else if (currentCol == 1)
            {
                g2d.setColor(Color.GREEN);
                rectangles.get(currentClicked).setBgColour(2);
                g2d.fill(rectangles.get(currentClicked));
            } else if (currentCol == 2)
            {
                g2d.setColor(Color.BLUE);
                rectangles.get(currentClicked).setBgColour(3);
                g2d.fill(rectangles.get(currentClicked));
            } else if (currentCol == 3)
            {
                g2d.setColor(Color.BLACK);
                rectangles.get(currentClicked).setBgColour(0);
                g2d.fill(rectangles.get(currentClicked));
            }
        } else if (typeOfShape == 2)
        {
            System.out.println("Sphere " + currentClicked + " was clicked last");
            int currentCol = spheres.get(currentClicked).getBgColour();
            System.out.println("Current bg colour is " + currentCol);
            if (currentCol == 0)
            {
                g2d.setColor(Color.RED);
                spheres.get(currentClicked).setBgColour(1);
                g2d.fill(spheres.get(currentClicked));
            } else if (currentCol == 1)
            {
                g2d.setColor(Color.GREEN);
                spheres.get(currentClicked).setBgColour(2);
                g2d.fill(spheres.get(currentClicked));
            } else if (currentCol == 2)
            {
                g2d.setColor(Color.BLUE);
                spheres.get(currentClicked).setBgColour(3);
                g2d.fill(spheres.get(currentClicked));
            } else if (currentCol == 3)
            {
                g2d.setColor(Color.BLACK);
                spheres.get(currentClicked).setBgColour(0);
                g2d.fill(spheres.get(currentClicked));
            }
        }
    }

    public static void main(String[] args)
    {
        new GUI_Two_Again().init();
    }


}
