import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

public class MyShape extends RectangularShape
{
    private static int rectangleCount = 0;
    private int shapeIndexID;
    private int bgColour = 0;

    public MyShape()
    {
        GUI_Two_Again.DrawingPanel.addShapeCount();
        this.shapeIndexID = rectangleCount - 1;
        System.out.println(this.shapeIndexID + " shapes");
    }


    @Override
    public double getX()
    {
        return 0;
    }

    @Override
    public double getY()
    {
        return 0;
    }

    @Override
    public double getWidth()
    {
        return 0;
    }

    @Override
    public double getHeight()
    {
        return 0;
    }

    @Override
    public boolean isEmpty()
    {
        return false;
    }

    @Override
    public void setFrame(double x, double y, double w, double h)
    {

    }

    @Override
    public Rectangle2D getBounds2D()
    {
        return null;
    }

    @Override
    public boolean contains(double x, double y)
    {
        return false;
    }

    @Override
    public boolean intersects(double x, double y, double w, double h)
    {
        return false;
    }

    @Override
    public boolean contains(double x, double y, double w, double h)
    {
        return false;
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at)
    {
        return null;
    }
}
