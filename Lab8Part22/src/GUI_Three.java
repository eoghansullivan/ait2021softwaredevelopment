import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class GUI_Three extends JFrame implements ActionListener
{
    // class variables
    private static Point clickCoord;
    private static int color = -1; // 0 black, 1 red, 2 green, 3 blue
    private static int shapeType = -1; //  1 square, 2 circle

    // panels
    private final DrawingPanel canvas = new DrawingPanel();

    // buttons
    private final JButton colBlack = new JButton("Black");
    private final JButton colRed = new JButton("Red");
    private final JButton colGreen = new JButton("Green");
    private final JButton colBlue = new JButton("Blue");
    private final JButton square = new JButton("Square");
    private final JButton circle = new JButton("Circle");

    // no argument constructor
    public GUI_Three()
    {

        this.setName("Shapes");
        this.setSize(1000, 700);
        this.setMinimumSize(new Dimension(1000, 700));
        this.setLayout(new GridBagLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * initializes GUI
     */
    public void init()
    {
        GridBagConstraints gbc = new GridBagConstraints();
        gridbagSetup(gbc);

        canvas.setBackground(Color.GRAY);
        addListeners();
        this.setVisible(true);
    }

    /**
     * sets up the buttons and canvas layout
     */
    public void gridbagSetup(GridBagConstraints gbc)
    {
        gbc.fill = GridBagConstraints.HORIZONTAL; gbc.ipadx = 170;
        gbc.gridx = 0; gbc.gridy = 0; this.add(colRed, gbc);
        gbc.gridx = 1; gbc.gridy = 0; this.add(colBlack, gbc);
        gbc.gridx = 2; gbc.gridy = 0; this.add(colBlue, gbc);
        gbc.gridx = 3; gbc.gridy = 0; this.add(colGreen, gbc);
        gbc.gridx = 1; gbc.gridy = 1; this.add(circle, gbc);
        gbc.gridx = 2; gbc.gridy = 1; this.add(square, gbc);
        gbc.gridx = 0; gbc.gridy = 2; gbc.gridwidth = 4;
        gbc.ipady = 550; this.add(canvas, gbc);
    }

    /**
     * adds action listeners to buttons
     */
    public void addListeners()
    {
        colRed.addActionListener(this);
        colBlack.addActionListener(this);
        colBlue.addActionListener(this);
        colGreen.addActionListener(this);
        circle.addActionListener(this);
        square.addActionListener(this);
    }

    /**
     * set click coordinates
     */
    public static void setClickCoord(Point p)
    {
        clickCoord = p;
    }

    /**
     * get click coordinates
     */
    public static Point getClickCoord()
    {
        return clickCoord;
    }

    /**
     * performs actions depending on which button is clicked
     */
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == colBlack)
        {
            System.out.println("Black selected");
            color = 0;
        } else if (e.getSource() == colRed)
        {
            System.out.println("Red selected");
            color = 1;
        } else if (e.getSource() == colGreen)
        {
            System.out.println("Green selected");
            color = 2;
        } else if (e.getSource() == colBlue)
        {
            System.out.println("Blue selected");
            color = 3;
        } else if (e.getSource() == square)
        {
            System.out.println("Square selected");
            shapeType = 1;
        } else if (e.getSource() == circle)
        {
            System.out.println("Circle selected");
            shapeType = 2;
        }
    }

    /**
     * gets the colour selected via button
     */
    public static int getColor()
    {
        return color;
    }

    /**
     * gets the shape type selected via button
     */
    public static int getShapeType()
    {
        return shapeType;
    }

    /**
     * canvas for placing shapes
     */
    class DrawingPanel extends JPanel
    {
        // Holds shapes
        private final ArrayList<ShapeHolder> shapes = new ArrayList<>();
        // index of current shape
        private int shapeIndex;
        // currently selected shape
        private Ellipse2D.Double currentSphere;
        private Rectangle2D.Double currentRectangle;
        // colours
        Color[] cols = {Color.BLACK, Color.RED, Color.GREEN, Color.BLUE};

        private boolean dragging = false;
        int x;
        int y;
        private boolean clicking = false;

        /**
         * no argument constructor, checks for mouse events
         */
        public DrawingPanel()
        {
            //mouse motion listener for dragging
            this.addMouseMotionListener(new MouseMotionAdapter()
            {
                @Override
                public void mouseDragged(MouseEvent e)
                {
                    if (clicking)
                    {
                        System.out.println("Dragging");
                        super.mouseDragged(e);
                        mouseDragging(e, shapes);
                    }

                }
            });

            //mouse listener for click or press events
            this.addMouseListener(new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    GUI_Three.setClickCoord(e.getPoint());
                    repaint();
                }

                @Override
                public void mousePressed(MouseEvent e)
                {
                    super.mousePressed(e);
                    clicking = true;
                }

                @Override
                public void mouseReleased(MouseEvent e)
                {
                    super.mouseReleased(e);
                    clicking = false;
                }
            });
        }

        /**
         * repaints all shapes, repaints dragged shape or creates new shape
         */
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            g2d = repaintPreviousShapes(shapes, g2d);

            if (!dragging)
            {
                if (GUI_Three.getColor() >= 0 && GUI_Three.getColor() <= 3)
                {
                    g2d.setColor(canvas.cols[GUI_Three.getColor()]);
                }
                createNewShape(g2d);
            } else
            {
                draggingShape(g2d);
            }
        }
    }

    /**
     * Iterates through the ArrayList of ShapeHolders and repaints with the correct shape type acknowledging dragging
     */
    public void mouseDragging(MouseEvent e, ArrayList<ShapeHolder> shapes)
    {
        canvas.x = e.getX();
        canvas.y = e.getY();

        for (ShapeHolder sh : shapes)
        {
            if (sh.getShapeType() == 0)
            {
                if (sh.returnRect().contains(e.getPoint()))
                {
                    canvas.currentRectangle = sh.returnRect();
                    canvas.shapeIndex = shapes.indexOf(sh);
                    canvas.dragging = true;
                    repaint();
                }
            } else if (sh.getShapeType() == 1)
            {
                if (sh.returnSph().contains(e.getPoint()))
                {
                    canvas.currentSphere = sh.returnSph();
                    canvas.shapeIndex = shapes.indexOf(sh);
                    canvas.dragging = true;
                    repaint();
                }
            }
        }
    }

    /**
     * Takes the colours and shapes from the ArrayList of Shapeholders and repaints them
     */
    public Graphics2D repaintPreviousShapes(ArrayList<ShapeHolder> shapes, Graphics2D g2d)
    {
        for (ShapeHolder sha : shapes)
        {
            g2d.setColor(canvas.cols[sha.getShapeColour()]);
            if (sha.getShapeType() == 0)
            {
                g2d.fill(sha.returnRect());

            } else if (sha.getShapeType() == 1)
            {
                g2d.fill(sha.returnSph());
            }
        }
        return g2d;
    }

    /**
     * Creates a new shape based on type selected and paints it to canvas
     */
    public void createNewShape(Graphics2D g2d)
    {
        if (GUI_Three.getClickCoord() != null)
        {
            if (GUI_Three.getShapeType() == 1)
            {
                ShapeHolder shCurrent = new ShapeHolder(new Rectangle2D.Double(), GUI_Three.getColor());
                canvas.currentRectangle = shCurrent.returnRect();
                canvas.currentRectangle.setRect(GUI_Three.getClickCoord().getX() - 25, GUI_Three.getClickCoord().getY() - 25, 50, 50);
                g2d.fill(canvas.currentRectangle);
                canvas.shapes.add(new ShapeHolder(canvas.currentRectangle, GUI_Three.getColor()));

            } else if (GUI_Three.getShapeType() == 2)
            {
                ShapeHolder shCurrent = new ShapeHolder(new Ellipse2D.Double(), GUI_Three.getColor());
                canvas.currentSphere = shCurrent.returnSph();
                canvas.currentSphere.setFrame(GUI_Three.getClickCoord().getX() - 25, GUI_Three.getClickCoord().getY() - 25, 50, 50);
                g2d.fill(canvas.currentSphere);
                canvas.shapes.add(new ShapeHolder(canvas.currentSphere, GUI_Three.getColor()));
            }
        }
    }

    /**
     * Repaints the dragged shape in the new location of the cursor
     */
    public void draggingShape(Graphics2D g2d)
    {
        if (canvas.shapes.get(canvas.shapeIndex) != null)
        {
            g2d.setColor(canvas.cols[canvas.shapes.get(canvas.shapeIndex).getShapeColour()]);

            if (canvas.shapes.get(canvas.shapeIndex).getShapeType() == 0)
            {
                canvas.currentRectangle = canvas.shapes.get(canvas.shapeIndex).returnRect();

                canvas.currentRectangle.setRect(canvas.x - 25, canvas.y - 25, 50, 50);
                g2d.fill(canvas.currentRectangle);
                canvas.dragging = false;

            } else if (canvas.shapes.get(canvas.shapeIndex).getShapeType() == 1)
            {
                canvas.currentSphere = canvas.shapes.get(canvas.shapeIndex).returnSph();
                canvas.currentSphere.setFrame(canvas.x - 25, canvas.y - 25, 50, 50);
                g2d.fill(canvas.currentSphere);
                canvas.dragging = false;
            }
        }
    }


    /**
     * Class for holding shapes (Rectangle2D.Double or Ellipse2D.Double)
     * Contains the colour and type information of the shape in ints
     * Has methods to return either shape object
     */
    static class ShapeHolder
    {
        private Rectangle2D.Double myTang;
        private Ellipse2D.Double mySph;
        private final int shapeColor;
        private final int shapeType;

        public ShapeHolder(Rectangle2D.Double tang, int shapeColor)
        {
            myTang = tang;
            this.shapeColor = shapeColor;
            this.shapeType = 0;
        }

        public ShapeHolder(Ellipse2D.Double sph, int shapeColor)
        {
            mySph = sph;
            this.shapeColor = shapeColor;
            this.shapeType = 1;
        }

        public Rectangle2D.Double returnRect()
        {
            return myTang;
        }

        public Ellipse2D.Double returnSph()
        {
            return mySph;
        }

        public int getShapeColour()
        {
            return shapeColor;
        }

        public int getShapeType()
        {
            return shapeType;
        }
    }

    public static void main(String[] args)
    {
        new GUI_Three().init();
    }
}
