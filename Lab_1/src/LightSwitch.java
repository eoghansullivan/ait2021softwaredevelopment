/*
 * 3. A class LightSwitch which has a status of on/off,
 * it also has a random chance to blow while it is being switched on.
 * Use the input of plus to turn on the light and minus to turn off the light.
 * You must constantly poll the keyboard for user input. The application ends when the light blows.
 */
public class LightSwitch
{
    private Boolean on, blown;

    // default constructor;
    public LightSwitch()
    {
        on = true;
        blown = false;
    }

    /*
     * Takes a string, if it's a + it turns the light on, if it's a - it turns it
     * off if it's already in either state, it notifies of this if it's blown it
     * notifies of this
     *
     */
    public void switchOnOrOff(String s)
    {
        if (!blown)
        {
            if (on)
            {
                if (s.equals("+"))
                {
                    System.out.println("The light is already on");
                } else if (s.equals("-"))
                {
                    System.out.println("The light turns off");
                    on = false;
                } else
                {
                    System.out.println("Incorrect character entered");
                }
            } else
            {
                if (s.equals("+"))
                {
                    System.out.println("The light turns on");
                    on = true;
                    // one in four chance to blow
                    if ((int) Math.round(Math.random() * 4) == 1)
                    {
                        blown = true;
                        System.out.println("The light blows.");
                    }

                } else if (s.equals("-"))
                {
                    System.out.println("The light is already off");
                } else
                {
                    System.out.println("Incorrect character entered");
                }
            }
        } else
        {
            System.out.println("Light is blown");
        }

    }

    // below are getter and setter methods
    public Boolean getOn()
    {
        return on;
    }

    public void setOn(Boolean on)
    {
        this.on = on;
    }

    public Boolean getBlown()
    {
        return blown;
    }

    public void setBlown(Boolean blown)
    {
        this.blown = blown;
    }

}
