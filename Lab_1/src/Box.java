/*
 * 2. A class Box complete with getter,setter and constructor  methods, it should have dimensions, name, status(full/empty)
 */
public class Box
{
    private int w, h, d;
    private String name;
    private Boolean full;

    // default constructor
    public Box()
    {
        w = 1;
        h = 1;
        d = 1;
        name = "boxy";
        full = true;
    }

    // constructor taking all variables as arguments
    public Box(int w, int h, int d, String name, Boolean full)
    {
        this.w = w;
        this.h = h;
        this.d = d;
        this.name = name;
        this.full = full;
    }

    /*
     * toString method
     */
    public String toString()
    {
        return "Box name: " + name + "\nBox width: " + w + "\nBox height: " + "\nBox depth: " + d + "\nBox full? "
                + full;
    }

    // all methods below are getters and setters for the class private variables
    public int getW()
    {
        return w;
    }

    public void setW(int w)
    {
        this.w = w;
    }

    public int getH()
    {
        return h;
    }

    public void setH(int h)
    {
        this.h = h;
    }

    public int getD()
    {
        return d;
    }

    public void setD(int d)
    {
        this.d = d;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Boolean getFull()
    {
        return full;
    }

    public void setFull(Boolean full)
    {
        this.full = full;
    }

}
