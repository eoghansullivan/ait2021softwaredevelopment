import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

public class Part_One extends JFrame implements ActionListener, ChangeListener, WindowListener {
    // main container
    private final JTabbedPane tabbedPane = new JTabbedPane();
    // panels
    private final JPanel p1 = new JPanel();
    private final JPanel p2 = new JPanel();
    private final JPanel p3 = new JPanel();

    // ------------panel 1
    // electoral area combo box on first panel
    private JComboBox<String> choices;
    // text pane to hold constituents based on electoral area
    private JTextPane textArea = new JTextPane();

    // ------------panel 2
    // table on second panel
    private JTable table = new JTable();
    // table sits inside scroll pane
    private JScrollPane scrollPane;
    // button to remove selected candidate
    private JButton removeButton = new JButton("Remove");

    // ------------panel 3 text fields, labels and button
    // text inputs
    private JTextField surNameInput = new JTextField(9);
    private JTextField firstNameInput = new JTextField(8);
    private JTextField partyInput = new JTextField(8);
    private JTextField areaInput = new JTextField(10);
    private JTextField addressInput = new JTextField(20);

    // labels
    private JLabel lastNameLabel = new JLabel("Surname");
    private JLabel firstNameLabel = new JLabel("First name");
    private JLabel partyLabel = new JLabel("Party");
    private JLabel areaLabel = new JLabel("Local Electoral Area");
    private JLabel addressLabel = new JLabel("Address");
    private JButton addButton = new JButton("Add");

    // ------------exit (save)
    // frame and panel appears on exit to confirm saving
    private SaveFrame frame2 = new SaveFrame();
    private JPanel onExit = new JPanel();

    // items on panel related to saving
    private JLabel saveQuestion = new JLabel("Would you like to save the file?");
    private JButton yesButt = new JButton("Yes");
    private JButton noButt = new JButton("No");

    // -------- layout and data
    private GridBagConstraints gbc = new GridBagConstraints();
    //read csv object
    private ReadCSV csv;

    // no argument constructor
    public Part_One() {
        // standard setup
        this.setTitle("GUI Part");
        this.setSize(750, 750);
        this.setLayout(new BorderLayout());
        this.addWindowListener(this);
    }

    public void init() {
        // sets up action listeners to buttons created in outer scope
        addButton.addActionListener(this);
        removeButton.addActionListener(this);

        // uses JFileChooser to select a file to be read
        File selectedFile = null;
        JFileChooser fileChooser = new JFileChooser();
        // sets the default path to the working directory
        fileChooser.setCurrentDirectory(new File("."));
        // uses an int to determine if the button clicked is the one to select a file and if so, gets that file
        int result = fileChooser.showOpenDialog(this.getContentPane());
        if (result == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
        }

        // initializes csv object
        csv = new ReadCSV(selectedFile);

        // creates a combo box with the model using generics of String
        DefaultComboBoxModel<String> options = new DefaultComboBoxModel<>();
        choices = new JComboBox<>(options);
        // adds an action listener to combo box
        choices.addActionListener(this);

        // adds the unique local electoral areas to the combo box
        for (LocalEleStat stat : csv.sortedArr()) {
            String area = stat.getLocalElectoralArea();

            if (options.getIndexOf(area) == -1) {
                options.addElement(area);
            }
        }


        //___________________________________________
        // Panel 1

        // sets layout for panel 1 and creates grid bag constraints to aid layout of elements
        p1.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // adds insets to left
        c.insets = new Insets(0, 5, 0, 5);

        // top left element space
        c.gridx = 0;
        c.gridy = 0;

        // constraints will fill the line horizontally
        c.fill = GridBagConstraints.HORIZONTAL;

        // adds the combo box element
        p1.add(choices, c);

        // under the combo box, sets constraint to fill the remaining available space
        c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 1.0;

        // adds the text area for constituents
        p1.add(textArea, c);

        // gets the selected electoral area and populates the text area with related candidates
        String area = (String) choices.getSelectedItem();
        setArea(area);

        //___________________________________________
        // Panel 2

        // sets layout and re-initialises grid bag constraints
        p2.setLayout(new GridBagLayout());
        c = new GridBagConstraints();

        // populates table from list
        updateTable();

        // sets up sorting on column headers of table
        table.setAutoCreateRowSorter(true);

        // initializes a scroll pane which contains the headings and the table and is scrollable
        scrollPane = new JScrollPane(table);

        // sets constraints to cover 90% of the page
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.weighty = 0.9;

        // adds scroll pane
        p2.add(scrollPane, c);

        // sets constraint to not resize
        c.fill = GridBagConstraints.NONE;

        // sets constraint to second row and to fill up the remaining 10% of the y axis
        c.gridy = 1;
        c.weighty = .1;

        // adds the remove button
        p2.add(removeButton, c);


        //___________________________________________
        // Panel 3

        // adds text inputs and labels to panel 3
        p3.setLayout(new GridBagLayout());

        p3.add(firstNameInput, gbc);

        gbc.gridx = 1;
        p3.add(surNameInput, gbc);

        gbc.gridx = 2;
        p3.add(partyInput, gbc);

        gbc.gridx = 3;
        p3.add(areaInput, gbc);

        gbc.gridx = 4;
        p3.add(addressInput, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        p3.add(firstNameLabel, gbc);

        gbc.gridx = 1;
        p3.add(lastNameLabel, gbc);

        gbc.gridx = 2;
        p3.add(partyLabel, gbc);

        gbc.gridx = 3;
        p3.add(areaLabel, gbc);

        gbc.gridx = 4;
        p3.add(addressLabel, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;

        p3.add(addButton, gbc);


        // Add panels and names to tabbed pane
        tabbedPane.add("Select Area", p1);
        tabbedPane.add("View All", p2);
        tabbedPane.add("Add New", p3);

        // adds a change listener to refresh content upon changing between tabs
        tabbedPane.addChangeListener(this);

        // adds the tabbed pane in a border layout to center it
        this.add(tabbedPane, BorderLayout.CENTER);

        this.setVisible(true);
    }

    /**
     * gets the column headings and initializes a table with them, iterates through list to populate table
     */
    private void updateTable() {
        String[] cols = csv.getHeadings();

        DefaultTableModel model = new DefaultTableModel(cols, 0);

        for (LocalEleStat stat : csv.getStats()) {
            model.addRow(new Object[]{stat.getNo(), stat.getSurname(), stat.getFirstName(), stat.getAddress(), stat.getParty(), stat.getLocalElectoralArea()});
        }

        table.setModel(model);
    }

    /**
     * matches the area string to the corresponding field from the list and adds a
     * representation of the candidates from that area to the text field as html
     */
    public void setArea(String area) {
        textArea.setText(" ");
        StringBuilder display = new StringBuilder("<html><table>");
        for (LocalEleStat stat : csv.getStats()) {
            if (stat.getLocalElectoralArea().equals(area)) {
                display.append(stat.toString());

            }

        }
        display.append("</table></html>");

        textArea.setContentType("text/html");
        textArea.setText(display.toString());
    }

    /**
     * action listeners
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        // for updating first tab when a new selection is made from combo box
        if (e.getSource() == choices) {
            String area = (String) choices.getSelectedItem();
            setArea(area);
        }

        // adds a candidate to the list in the correct format
        if (e.getSource() == addButton) {
            String toAdd = csv.getNextNum() + ",";
            toAdd += surNameInput.getText().replaceAll(",", " ") + ",";
            toAdd += firstNameInput.getText().replaceAll(",", " ") + ",";
            toAdd += "\"" + addressInput.getText().replaceAll("\"", " ") + "\",";
            toAdd += partyInput.getText().replaceAll(",", " ") + ",";
            toAdd += areaInput.getText().replaceAll(",", " ") + ",,,,,";
            csv.addStat(toAdd);
        }

        // deletes selected a candidate from the list
        if (e.getSource() == removeButton) {

            //String value = table.getModel().getValueAt(table.getSelectedRow(), 0).toString();
            // fixed!
            String value = table.getValueAt(table.getSelectedRow(), 0).toString();
            csv.removeStat(value);
            updateTable();
        }

    }

    /**
     * updates contents of particular tab when that tab is selected
     */
    @Override
    public void stateChanged(ChangeEvent changeEvent) {

        JTabbedPane temp = (JTabbedPane) changeEvent.getSource();

        if (temp.getSelectedIndex() == 0) {
            String area = (String) choices.getSelectedItem();
            setArea(area);
        } else if (temp.getSelectedIndex() == 1) {
            updateTable();
        }
    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    /**
     * opens a new frame with a panel with buttons option to save on exit
     */
    @Override
    public void windowClosing(WindowEvent windowEvent) {
        frame2.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame2.add(saveQuestion);
        frame2.add(onExit);
        frame2.setLayout(new FlowLayout());
        onExit.add(yesButt);
        yesButt.addActionListener(frame2);
        onExit.add(noButt);
        noButt.addActionListener(frame2);
        frame2.pack();
        frame2.setVisible(true);

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {


    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }

    /**
     * frame for saving on exit
     */
    class SaveFrame extends JFrame implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == yesButt) {
                csv.writeFile();
            } else {
                System.exit(0);
            }
        }
    }

    public static void main(String[] args) {
        new Part_One().init();
    }


}
