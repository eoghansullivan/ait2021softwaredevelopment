import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class ReadCSV {

    private ArrayList<LocalEleStat> stats = new ArrayList<>();
    private String[] headings;

    public ReadCSV(File f) {
        try {

            // the below line creates a scanner object from the Scanner class called sc from the File f
            Scanner sc = new Scanner(f);

            sc.nextLine();//skip first heading
            headings = sc.nextLine().split(",");//add second row to headings

            // adds remaining lines to arraylist as localelestat objects
            while (sc.hasNextLine()) {
                try {
                    stats.add(new LocalEleStat(sc.nextLine()));
                } catch (IllegalArgumentException ex) {
                    // do nothing
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * copies the arraylist and sorts is
     * returns that sorted arraylist
     */
    public ArrayList<LocalEleStat> sortedArr() {
        ArrayList<LocalEleStat> sorted = new ArrayList<>(stats);
        LocalEleStat.sort(sorted);
        return sorted;
    }

    // getters
    public ArrayList<LocalEleStat> getStats() {
        return stats;
    }

    public String[] getHeadings() {
        return headings;
    }

    // adds a new localelestat from list
    public void addStat(String s) {
        stats.add(new LocalEleStat(s));
    }

    // removes a localelestat list
    public void removeStat(String s) {
        int i = -1;
        for (LocalEleStat stat : stats) {
            if (stat.getNo().equals(s)) {
                i = stats.indexOf(stat);
                break;
            }
        }

        if (i != -1) {
            System.out.println("Deleting " + stats.get(i).toString());
            stats.remove(i);
        }

    }

    // gets the highest number in the list and returns 1 higher for use in adding next item
    public int getNextNum() {
        int num = 0;
        for (LocalEleStat stat : stats) {
            if (num < Integer.parseInt(stat.getNo())) {
                num = Integer.parseInt(stat.getNo());
            }
        }
        return num + 1;
    }

    // writes arraylist to file
    public void writeFile() {
        try {
            // uses date in file name
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH_mm_ss");
            Date date = new Date();
            File f = new File("output " + formatter.format(date) + ".csv");
            // prints file name
            System.out.println(f.toString());
            // prints if it got created
            System.out.println("it is " + f.createNewFile() + " that a file got created");

            // created print writer object and adds heading lines to file
            PrintWriter pw = new PrintWriter(f);
            pw.println("NOMINATIONS FOR LOCAL ELECTIONS 2009,,,,,,,,,,");
            pw.println("No ,Surname,First Name,Address,Party,Local Electoral Area,,,,,");

            // writes each other line to the file
            for (LocalEleStat stat : stats) {
                pw.println(stat.toCSV());
            }
            // closes print writer
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }


}
