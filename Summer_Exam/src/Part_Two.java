import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;

public class Part_Two extends JFrame implements ActionListener {

    private DrawingCanvas canvas = new DrawingCanvas();
    private JButton resetShape = new JButton("reset");


    public Part_Two() {
        this.setSize(new Dimension(700, 700));
        this.setLayout(new GridBagLayout());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        canvas.setBackground(Color.BLUE);
    }

    public void init() {

        GridBagConstraints gbc = new GridBagConstraints();

        resetShape.addActionListener(this);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 1.0;
        gbc.weighty = 9.9;

        this.add(canvas, gbc);
        this.addMouseListener(canvas);
        this.addMouseMotionListener(canvas);

        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.weighty = 0.1;
        this.add(resetShape, gbc);
        this.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == resetShape) {
            canvas.resetShape();
        }
    }

    class DrawingCanvas extends JPanel implements MouseListener, MouseMotionListener {
        private Point sp_x; // starting point for x
        private Point p;
        private double scale = 4.0;
        private double colour_axis = 1.0;
        private Color currentCol = Color.black;
        private Rectangle2D.Double placed;
        private boolean holding;


        public void resetShape() {
            scale = 4;
            colour_axis = 1;
            currentCol = Color.black;
            repaint();
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            double width = this.getWidth();
            double height = this.getHeight();
            g2d.setColor(currentCol);
            placed = new Rectangle2D.Double(width / 2 - (25 * scale), height / 2 - (25 * scale), 50 * scale, 50 * scale);
            g2d.fill(placed);

        }

        @Override
        public void mouseClicked(MouseEvent e) {


        }

        @Override
        public void mousePressed(MouseEvent e) {
            if (placed.contains(e.getPoint())) {
                holding = true;
                System.out.println("HOLDING");
                sp_x = e.getPoint();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            System.out.println("NOT HOLDING");
            holding = false;
        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (holding) {
                p = e.getPoint();
                colour_axis = 0;

                scale = 4 + (p.x - sp_x.x) / 10; // adjusts in increments of 10, so it's not too sensitive

                // v strange construct
                colour_axis = colour_axis + (p.y - sp_x.y) / 10;
                System.out.println(colour_axis);
                if(colour_axis < 0)
                {
                    colour_axis *= -1;
                }
                if (colour_axis > - 11 && colour_axis < 11) {
                    currentCol = Color.black;
                    colour_axis = 11;
                } else if (colour_axis > 10 && colour_axis < 21 ) {
                    currentCol = Color.cyan;
                    colour_axis = 21;
                } else if (colour_axis > 20) {
                    currentCol = Color.green;
                    colour_axis = 31;
                } else if (colour_axis > 30) {
                    currentCol = Color.red;
                    colour_axis = 0;
                }
                repaint();
            }
        }

        @Override
        public void mouseMoved(MouseEvent e) {

        }
    }

    public static void main(String[] args) {
        new Part_Two().init();
    }


}
