import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LocalEleStat implements Comparable<LocalEleStat> {

    private String no, surname, firstName, party, localElectoralArea;
    private Address address;
    private static int sortingMethod = 3;

    // constructor, parses string into a usable format
    public LocalEleStat(String s) {
        try {

            Scanner sc = new Scanner(s);

            sc.useDelimiter("\"");

            String[] part1 = sc.next().split(",");
            address = new Address(sc.next());
            String[] part3 = sc.next().split(",");

            no = part1[0];
            surname = encode(part1[1]);
            firstName = encode(part1[2]);

            party = encode(part3[1]);
            localElectoralArea = part3[2];
        } catch (Exception e) {
            //ignore exceptions
            throw new IllegalArgumentException("doesnt fit");
        }
    }

    // encodes to utf_8 to attempt to preserve special characters
    private String encode(String s) {
        ByteBuffer buffer = StandardCharsets.UTF_8.encode(s);
        return StandardCharsets.UTF_8.decode(buffer).toString();
    }

    // prepares object to be saved to file (annoying fun regex)
    public String toCSV() {
        return String.format("%s,%s,%s,%s,%s,%s", no, surname, firstName, "\"" + getAddress().toString().replaceAll("\\[|]|  ", "") + "\"", party, localElectoralArea);
    }

    @Override
    public String toString() {


        return String.format("<tr><td>%s</td><td>%s</td><td>%s</td></tr>", (surname + "," + firstName), party, localElectoralArea);
        //return surname + ", \t" + firstName + "\t(" + party +") \t\t\t" + localElectoralArea;
    }

    // (go)getters
    public String getNo() {
        return no;
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getParty() {
        return party;
    }

    public String getLocalElectoralArea() {
        return localElectoralArea;
    }

    public Address getAddress() {
        return address;
    }

    // never used
    public static void setSortingMethod(int sorting) {
        sortingMethod = sorting;
    }

    // compares on all fields
    @Override
    public int compareTo(LocalEleStat o) {
        if (sortingMethod == 0) {
            if (this.firstName.equalsIgnoreCase(o.getFirstName())) {
                return 1;
            }
        }
        if (sortingMethod == 1) {

            if (this.surname.compareTo(o.surname) < 0) {
                return 1;
            }

        }
        if (sortingMethod == 2) {
            if (this.party.compareTo(o.party) < 0) {
                return 1;
            }
        }
        if (sortingMethod == 3) {
            if (this.localElectoralArea.compareTo(o.localElectoralArea) < 0) {
                return 1;
            }
        }
        if (sortingMethod == 4) {
            if (this.address.toString().compareTo(o.address.toString()) < 0) {
                return 1;
            }
        }
        if (sortingMethod == 5) {
            if (this.no.compareTo(o.no) < 0) {
                return 1;
            }
        }
        return 0;
    }

    // basic sort
    public static void sort(ArrayList<LocalEleStat> arrayToSort) {
        for (int i = 0; i < arrayToSort.size() - 1; i++) {
            int leastIndex = i;

            for (int j = i + 1; j < arrayToSort.size(); j++) {
                if (arrayToSort.get(j).compareTo(arrayToSort.get(leastIndex)) == 1) {
                    leastIndex = j;
                }

            }
            if (leastIndex != i) {
                swap(arrayToSort, i, leastIndex);
            }
        }
    }
    // simple swap
    public static void swap(ArrayList<LocalEleStat> a, int i, int j) {
        LocalEleStat temp = a.get(i);
        a.set(i, a.get(j));
        a.set(j, temp);
    }

    // internal address class (composition)
    class Address {
        String[] lines;

        public Address(String s) {


            lines = s.split(",");
        }


        @Override
        public String toString() {
            return Arrays.toString(lines);

        }
    }
}
