import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Arc2D;
import java.util.ArrayList;

public class TouchShapes extends JFrame {


    private CanvasPanel drawingArea = new CanvasPanel();

    public TouchShapes()
    {
        setTitle("Touch shapes");
        setSize(400,400);
    }

    public void init()
    {
        add(drawingArea);
        setVisible(true);
    }

    class CanvasPanel extends JPanel implements MouseListener
    {
        private ArrayList<Shape> shapes =  new ArrayList<>();

        public CanvasPanel()
        {
            addMouseListener(this);

            shapes.add(new Arc2D.Double(50,50,230,270,45,90,Arc2D.OPEN));
        }

        public void doSomething(Graphics g)
        {
            Graphics2D g2d = (Graphics2D) g;

            for(int i = 0; i< shapes.size(); i++)
            {
                Shape shape = shapes.get(i);
                Color color = Color.blue;

                g2d.setColor(color);
                g2d.fill(shape);
            }
        }


        @Override
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            doSomething(g);
        }


        @Override
        public void mouseClicked(MouseEvent e) {
            for(int i = 0; i < shapes.size(); i++)
            {
                Shape shape = shapes.get(i);
                if(shape.contains(e.getPoint()))
                {
                    System.out.println("shape clicked");
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }

    public static void main(String[] args) {
        new TouchShapes().init();
    }
}
