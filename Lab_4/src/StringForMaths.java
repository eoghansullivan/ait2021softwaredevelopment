import java.util.Scanner;

public class StringForMaths
{

    public static void main(String[] args)
    {
        //copy codestream column here into yoke ""
        String yoke = "";
        yoke = yoke.replaceAll("[^a-zA-Z0-9]", " ");
        Scanner sc = new Scanner(yoke);
        //85 is amount of elements
        int[] nums = new int[85];
        String out = "";

        for (int i = 0; i < 85; i++)
        {
            nums[i] = sc.nextInt();
            out += "(";
            out += nums[i];
            out += ")";
        }

        System.out.println(out);

    }

}
