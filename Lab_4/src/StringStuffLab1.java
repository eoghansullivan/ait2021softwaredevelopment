public class StringStuffLab1
{

    public static void main(String[] args)
    {
        if (args[0] != null)
        {
            String s = args[0];
            toString2(s);
        }

    }

    /**
     * @param s String in Parses the string char by char and totals a vowel count to
     *          return
     * @return vowel count
     */
    static int numberOfVowels(String s)
    {
        int vow = 0;
        s = s.toLowerCase();
        for (char i : s.toCharArray())
        {
            if (i == 'a' || i == 'e' || i == 'i' || i == 'o' || i == 'u')
            {
                vow++;
            }
        }
        return vow;
    }

    /**
     * @param x Takes the string and prints the Length, number of vowels, first and
     *          last letters
     */
    static void toString2(String s)
    {
        System.out.print("Length = " + s.length() + "\nNumber of Vowels: " + numberOfVowels(s) + "\nFirst letter: "
                + s.charAt(0) + "\nLast Letter: ");
        if (java.lang.Character.isSpaceChar(s.charAt(s.length() - 1)))
        {
            System.out.println(s.charAt(s.length() - 2));
        } else
        {
            s.charAt(s.length() - 1);
        }
        System.out.println();

    }

}
