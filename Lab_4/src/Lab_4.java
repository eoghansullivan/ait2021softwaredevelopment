import java.io.*;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Lab_4
{

    public static void main(String[] args)
    {
        // 1. Read in from last weeks text file and print out every 5th word.
        Scanner sc = new Scanner(System.in);
        File file = new File("Lab 3.txt");

        System.out.println("//Part 1");
        System.out.println(getWordsAtPosition(file, 5, 0) + "\n");

        /*
         * 2. Create an array of 5 files (do not hard code), put one fifth of the words
         * from the text file in each file
         */
        System.out.println("//Part 2");
        System.out.println("How many files do you want to make/split contents into?");

        int amount = sc.nextInt();

        createFiles(file, amount);
        sc.close();

    }

    /**
     * takes a file to read, an increment and an index, scans the file and builds a
     * string containing the elements at the specified increment beginning at the
     * specified index, returns this string
     *
     * @param f         - file to read from
     * @param increment - increment between each word
     * @param index     - beginning position
     * @return string
     */
    public static String getWordsAtPosition(File f, int increment, int index)
    {
        String test = "", toPrint = "";
        try
        {
            /*
             * iterates inputs from scanner scanning file and creates a string array
             * separated at spaces
             */
            Scanner sc = new Scanner(f);
            while (sc.hasNext())
            {
                test += sc.next() + " ";
            }
            String[] arr = test.trim().split("\\s+");

            /*
             * adds each word to a string to return in specified increments beginning at
             * index specified
             */
            for (int i = index; i < arr.length; i += increment)
            {
                arr[i] = arr[i].replaceAll("[^a-zA-Z0-9]", "");
                toPrint += arr[i];
                toPrint += " ";

            }
            sc.close();

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return toPrint;
    }

    /**
     * populates with content of parameter file split at increments of the number of
     * files specified
     *
     * @param file  - the file to read from
     * @param count - the amount of files to create creates a number of files,
     */
    public static void createFiles(File file, int count)
    {
        File[] files = new File[count];
        String[] contains = new String[files.length];

        /*
         * creates specified amount of files and writes into them splitting the original
         * file read into that amount of words incremented by same amount of words
         */
        for (int i = 0; i < count; i++)
        {
            files[i] = new File("file" + i + ".txt");
            contains[i] = getWordsAtPosition(file, files.length, i);
            BufferedWriter output = null;

            try
            {
                output = new BufferedWriter(new FileWriter(files[i]));
                output.write(contains[i]);
                System.out.println("file" + i + ".txt created");
            } catch (IOException e)
            {
                e.printStackTrace();
            } finally
            {
                if (output != null)
                {
                    try
                    {
                        output.close();
                    } catch (IOException f)
                    {
                        f.printStackTrace();
                    }
                }
            }
        }

        try
        {
            /*
             * 3. Randomly swap the contents of all files,( eg file1 now contains the
             * contents of file3 ....), finally printing the statistics for each of the
             * files
             */
            System.out.println();
            System.out.println("//Part 3");

            shuffleFiles(contains);

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * takes an array of strings, shuffles an array of ints of the length of the
     * string array, prints the string array to one file per element in the order of
     * the shuffle
     *
     * @param wordsForFiles - array of strings to shuffle to be printed to file
     * @throws FileNotFoundException
     */
    public static void shuffleFiles(String[] wordsForFiles) throws FileNotFoundException
    {
        PrintWriter[] pw = new PrintWriter[wordsForFiles.length];
        int[] shufd = shuff(wordsForFiles.length);

        System.out.println();

        for (int i = 0; i < wordsForFiles.length; i++)
        {
            pw[i] = new PrintWriter(new File("file" + i + ".txt"));
            pw[i].println(wordsForFiles[shufd[i]]);
            StringStuffLab1.toString2(wordsForFiles[shufd[i]]);
            pw[i].close();

        }

    }

    /**
     * takes an int and returns an array, via a random iterated swap, populated by a
     * random order of ints with the same length as the specified int
     *
     * @param amount - amount of array elements to shuffle
     * @return ar - shuffled array
     */
    public static int[] shuff(int amount)
    {
        int[] ar = new int[amount];
        for (int i = 0; i < amount; i++)
        {
            ar[i] = i;
        }
        Random rnd = ThreadLocalRandom.current();
        for (int i = 0; i < ar.length; i++)
        {
            int index = rnd.nextInt(i + 1);
            int a = ar[index];

            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;
    }

}
