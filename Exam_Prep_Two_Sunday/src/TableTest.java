import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TableTest extends JFrame implements ActionListener {
    private JButton checker = new JButton("Check");
    private JPanel myPanel = new JPanel();
    private JTable table = new JTable();
    private JScrollPane scrollPane;

    public TableTest() {
        checker.addActionListener(this);
        this.setLayout(new BorderLayout());
        this.add(myPanel);
        //this.setSize(new Dimension(200,200));
        myPanel.add(checker);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        updateTestTable();
        //myPanel.add(table);
        scrollPane = new JScrollPane(table);
        myPanel.add(scrollPane);
        this.pack();

    }

    public static void main(String[] args) {
        new TableTest();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == checker)
        {
            //String value = table.getModel().getValueAt(table.getSelectedRow(), 0).toString();
            String value = table.getValueAt(table.getSelectedRow(), 0).toString();
            System.out.println(value);
        }
    }

    private void updateTestTable() {
        String[] cols = {"One", "Two", "Three", "Four", "Five"};
        DefaultTableModel model = new DefaultTableModel(cols, 0);

        model.addRow(new Object[]{"R1C1", "R1C2", "R1C3", "R1C4", "R1C5"});
        model.addRow(new Object[]{"R2C1", "R2C2", "R2C3", "R2C4", "R2C5"});
        model.addRow(new Object[]{"R3C1", "R3C2", "R3C3", "R3C4", "R3C5"});
        model.addRow(new Object[]{"R4C1", "R4C2", "R4C3", "R4C4", "R4C5"});
        model.addRow(new Object[]{"R5C1", "R5C2", "R5C3", "R5C4", "R5C5"});
        table.setModel(model);
        table.setAutoCreateRowSorter(true);
    }
}
